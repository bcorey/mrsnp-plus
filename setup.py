#!/usr/bin/env python
# encoding: utf-8

import setuptools

with open("README.md", "r") as readme:
    long_description = readme.read()

setuptools.setup(
    name="mrsnp-plus",
    version="1.0.3",
    author="Brendan Corey",
    author_email="brendan.w.corey.ctr@mail.mil",
    description="MRSN bacterial variant analysis and annotation pipeline",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/bcorey/mrsnp-plus",
    license="GPLv3",
    packages=setuptools.find_packages(),
    package_data={
        "mrsnp_plus": ["resources/*.fa"]
    },
    entry_points={'console_scripts': ['mrsnp+=mrsnp_plus.run:mrsnp_plus']
                  },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
    python_requires='>=3.5',
    zip_safe=False
)
