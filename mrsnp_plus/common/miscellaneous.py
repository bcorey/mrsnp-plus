#!/usr/bin/env python
# encoding: utf-8
import distutils
import subprocess
import sys
from pathlib import Path

from mrsnp_plus.common import log


def path_clean(file_path):
    """
    sometimes users are careless. This method will use the pathLib module to convert user input into full posix path
    objects to avoid confusion downstream
    """

    return Path(str(file_path)).expanduser().resolve()


def file_extension_check(file_path=None, file_extension=None):
    """
    verify that the file provided has the correct extension
    """
    for extension in file_extension:
        if extension in file_path.suffixes:
            return True
    return False


def file_check(file_path=None, file_or_directory=None):
    """
    checks to see if the file associated with the individual_sequencing_sample object exists in the file system at the
    specified PATH. Returns True if yes and False if no. Also return False if the attribute does not exist
    """
    if file_or_directory == 'file':
        if file_path:
            if file_path.exists():
                return True
            else:
                return False
        else:
            return False

    if file_or_directory == "directory":
        if file_path:
            if file_path.is_dir():
                return True
            else:
                return False
        else:
            return False


def check_for_dependency(program):
    """
    check_for_dependency() will use distutils.spawn to determine if the input dependency is available on the users PATH
    returns True if the dependency is located, False otherwise
    """
    if distutils.spawn.find_executable(str(program)):
        return True
    else:
        return False


def quit_with_error(logger, message):
    """
    Displays the given message and ends the program's execution.
    """
    logger.log(log.bold_red('Error: ') + message, 0, stderr=True)
    sys.exit(1)


def query_yes_no(question, default=None):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def round_down(num, divisor):
    return num - (num % divisor)


def ascii_art(logo=None, author=None, version=None, logo_only=False):
    """
    Print out the ascii art logo requested by the user
    :param logo: the name of the logo to be printed
    """

    if logo == 'mrsnp':
        ascii_logo = log.bold_red("""

     .----------------.  .----------------.  .----------------.  .-----------------. .----------------.  .----------------. 
    | .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
    | | ____    ____ | || |  _______     | || |    _______   | || | ____  _____  | || |   ______     | || |      _       | |
    | ||_   \  /   _|| || | |_   __ \    | || |   /  ___  |  | || ||_   \|_   _| | || |  |_   __ \   | || |     | |      | |
    | |  |   \/   |  | || |   | |__) |   | || |  |  (__ \_|  | || |  |   \ | |   | || |    | |__) |  | || |  ___| |___   | |
    | |  | |\  /| |  | || |   |  __ /    | || |   '.___`-.   | || |  | |\ \| |   | || |    |  ___/   | || | |___   ___|  | |
    | | _| |_\/_| |_ | || |  _| |  \ \_  | || |  |`\____) |  | || | _| |_\   |_  | || |   _| |_      | || |     | |      | |
    | ||_____||_____|| || | |____| |___| | || |  |_______.'  | || ||_____|\____| | || |  |_____|     | || |     |_|      | |
    | |              | || |              | || |              | || |              | || |              | || |              | |
    | '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
     '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------' 
         
     """)
    if logo_only:
        return ascii_logo


    print(ascii_logo)
    print(log.bold_red("\t\t\t\tMRSN Single Nucleotide Polymorphism (SNP) Analysis Tool"))
    print(log.bold_red("\t\t\t\t\t" + str(author)))
    print(log.bold_red("\t\t\t\t\t\tv" + str(version)))
    print('\n')


def get_version(args):
    print(log.bold('Installed version: ' + args.version))


def parallel_compression_handler(target_files, compress=False, decompress=False, compression_type=None, logger=None, cores=1):
    """
    Use GNU parallel to handle the simultaneous compression/decompression of all of the files in a list of files.
    """
    # verify that exactly one action was specified
    try:
        assert (compress and not decompress) or (not compress and decompress)
    except AssertionError:
        quit_with_error(logger, 'Either compress OR decompress must be specified for compression_handler()!')
        raise

    # verify that the compression_type passed is one that is acceptable
    try:
        assert compression_type == 'gzip' or compression_type == 'bzip2'
    except AssertionError:
        quit_with_error(logger, 'Either gzip or bzip2 must be specified for compression_handler()!')
        raise

    # set the actions based on the compression_type and compress vs decompress
    if compression_type == 'gzip':
        if compress:
            action = 'gzip'
        if decompress:
            action = 'gunzip'
    if compression_type == 'bzip2':
        if compress:
            action = 'bzip2'
        if decompress:
            action = 'bunzip2'

    # Based on the action, see if the action is required (i.e. can't gunzip a non-.gz file). To track this, we will
    # create a new list (action_required) which will just store True for action required and False for no action
    # required. We will also create a third list of the expected output file path post action or the current file path
    # if no action is required. Finally we will zip these two lists to the target_files list
    action_required = []
    output_paths = []
    for target_file in target_files:
        if action == 'gzip':
            if target_file.suffix == '.gz':
                action_required.append(False)
                output_paths.append(target_file)
            else:
                action_required.append(True)
                output_path = target_file.parent / (target_file.name + '.gz')
                output_paths.append(output_path)
        if action == 'gunzip':
            if target_file.suffix != '.gz':
                action_required.append(False)
                output_paths.append(target_file)
            else:
                action_required.append(True)
                output_path = target_file.parent / (target_file.name[:-3])
                output_paths.append(output_path)
        if action == 'bzip2':
            if target_file.suffix == '.bz2':
                action_required.append(False)
                output_paths.append(target_file)
            else:
                action_required.append(True)
                output_path = target_file.parent / (target_file.name + '.bz')
                output_paths.append(output_path)
        if action == 'bunzip2':
            if target_file.suffix != '.bz2':
                action_required.append(False)
                output_paths.append(target_file)
            else:
                action_required.append(True)
                output_path = target_file.parent / (target_file.name[:-4])
                output_paths.append(output_path)

    # validate the target files exist if an action is required. If one is missing and we are decompressing, this is
    # fatal as we won't be able to produce some form of output. If compress this gets a warning as it just means we
    # are doing a bad job cleaning
    validated_target_files = []
    for target_file in target_files:
        try:
            assert file_check(file_path=target_file, file_or_directory='file')
            validated_target_files.append(True)
        except AssertionError:
            if decompress:
                quit_with_error(logger, 'Failed to locate target file at specified path: ' + str(target_file))
            if compress:
                logger.log(log.bold_red(
                    'WARNING: Unable to locate the target file: {}, so compression can not be performed'.format(
                        str(target_file))))
                validated_target_files(False)

    # zipping all of these lists together should give us tuples that we can use for for a) calling parallel compresssion
    # b) verifying output and c) returning the new paths in a list with the same order as target_files
    target_file_tuple = list(zip(target_files, action_required, output_paths, validated_target_files))

    # run the action on all files where action_required is true and validated_target_files is true using parallel
    files_for_processing = [str(target_file[0]) for target_file in target_file_tuple if
                            (target_file[1] == True and target_file[3] == True)]

    if len(files_for_processing) > 0:

        files_as_string = ' '.join(files_for_processing)

        if cores > len(files_for_processing):
            cores = len(files_for_processing)

        parallel_process = subprocess.run(
            [
                'parallel -j ' + str(cores) + ' ' + action + ' {} ::: ' + files_as_string
                ],
            shell=True,
            stderr=subprocess.PIPE
        )

        # check the return code
        try:
            assert parallel_process.returncode == 0
        except AssertionError:
            quit_with_error(logger,
                            'parallel compression/decompression returned a non-zero returncode when handling {}'.format(
                                files_as_string))

    return [target_file[2] for target_file in target_file_tuple]


def compression_handler(compress=False, decompress=False, target_file=None, compression_type=None, logger=None):
    """
    non-object oriented compression handling method. Either compress or decompress must be True. Method will determine
    whether or not compression/decompression is required and if so it will use the subprocess module to perform the
    requested operations
    """
    # verify that exactly one action was specified
    try:
        assert (compress and not decompress) or (not compress and decompress)
    except AssertionError:
        quit_with_error(logger, 'Either compress OR decompress must be specified for compression_handler()!')
        raise

    # verify that exactly one action was specified
    try:
        assert compression_type == 'gzip' or compression_type == 'bzip2'
    except AssertionError:
        quit_with_error(logger, 'Either gzip or bzip2 must be specified for compression_handler()!')
        raise

    # verify that file(s) were included to be worked on
    try:
        assert target_file
    except AssertionError:
        quit_with_error(logger, 'a target file must be passed to compression_handler()!')
        raise

    # validate the target file
    try:
        assert file_check(file_path=target_file, file_or_directory='file')
    except AssertionError:
        quit_with_error(logger,
                        'Failed to locate target file at specified path: ' + str(target_file))

    target_file = path_clean(file_path=target_file)

    file_path_to_return = None

    # handles .gz compression/decompression of files
    if compression_type == 'gzip':
        # gzip compression
        if compress:
            if not target_file.name.endswith('.gz'):
                subprocess.run(['gzip', str(target_file)])
                file_path_to_return = (str(target_file) + '.gz')
            else:
                file_path_to_return = (str(target_file))
        # gunzip decompression
        if decompress:
            if target_file.name.endswith('.gz'):
                subprocess.run(['gunzip', str(target_file)])
                file_path_to_return = ('.'.join(str(target_file).split('.')[:-1]))
            else:
                file_path_to_return = (str(target_file))

    # handles .bzip2 compression/decompression of files
    if compression_type == 'bzip2':
        # bzip2 compression
        if compress:
            if not target_file.name.endswith('.bz') or not target_file.name.endswith(
                    '.bz2') or not target_file.name.endswith('.bzip2'):
                subprocess.run(['bzip2', str(target_file)])
                file_path_to_return = (str(target_file) + '.bz')
            else:
                file_path_to_return = (str(target_file))
        # bunzip2 decompression
        if decompress:
            if target_file.name.endswith('.bz') or target_file.name.endswith('.bz2') or target_file.name.endswith(
                    '.bzip2'):
                subprocess.run(['bunzip2', str(target_file)])
                file_path_to_return = ('.'.join(str(target_file).split('.')[:-1]))
            else:
                file_path_to_return = (str(target_file))

    file_path_to_return = path_clean(file_path=file_path_to_return)

    # validate the file path to return
    try:
        assert file_check(file_path=file_path_to_return, file_or_directory='file')
    except AssertionError:
        quit_with_error(logger,
                        'compression_handler() failed to generate the following file: ' + str(target_file))

    return file_path_to_return


# check_required_programs will make sure that the script is able to see all of the executables required to run
def check_required_programs(args, logger):
    if not distutils.spawn.find_executable('bowtie2-build'):
        quit_with_error(logger, 'could not find bowtie2-build')
    if not distutils.spawn.find_executable('bowtie2'):
        quit_with_error(logger, 'could not find bowtie2')
    if not distutils.spawn.find_executable('bbduk.sh'):
        quit_with_error(logger, 'could not find bbduk.sh')
    if not distutils.spawn.find_executable('repair.sh'):
        quit_with_error(logger, 'could not find repair.sh')
    if not distutils.spawn.find_executable('samtools'):
        quit_with_error(logger, 'could not find samtools')
    if not distutils.spawn.find_executable('bcftools'):
        quit_with_error(logger, 'could not find bcftools')
    if not distutils.spawn.find_executable('fuse.sh'):
        quit_with_error(logger, 'could not find fuse.sh')
    if args.gubbins:
        if not distutils.spawn.find_executable('run_gubbins.py'):
            quit_with_error(logger, 'could not find gubbins')
