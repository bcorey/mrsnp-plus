#!/usr/bin/env python3
# encoding: utf-8
import json
import subprocess

from mrsnp_plus.common.miscellaneous import file_check, quit_with_error


class CheckPoint:

    def __init__(self, name, analysis_type, logger, checkpoint_directory):
        self.name = str(name)
        self.logger = logger

        # type defines the nature of what is being tracked. It can be a sample or a reference. The content of the
        # checkpoint object and the associated checkpoint JSON file are governed by the type
        try:
            assert analysis_type in ['sample', 'reference', 'group']
            self.analysis_type = str(analysis_type)
        except AssertionError:
            quit_with_error(logger, 'Error: the value for type passed to CheckPoint must be either sample of refererence, not {}'.format(str(analysis_type)))

        # checkpoint directory stores the path to the directory where the checkpoint file is stored
        self.checkpoint_directory = checkpoint_directory

        # using initialize_checkpoint_file() check to see if the checkpoint file already exists or if a new one needs
        # to be created
        self.checkpoint_file = self.initialize_checkpoint_file()

        # perform an initial read of the checkpoint_file to set the self.checkpoint_dict
        self.checkpoint_dict = None
        self.read_checkpoint_file()

    def initialize_checkpoint_file(self):

        checkpoint_file = self.checkpoint_directory / ('{}.checkpoint.JSON'.format(self.name))

        # check to see if the checkpoint file exists
        try:
            assert not file_check(file_path=checkpoint_file, file_or_directory='file')
        except AssertionError:
            return checkpoint_file

        # make the checkpoint directory if it doesn't already exist
        if not self.checkpoint_directory.is_dir():
            subprocess.run(['mkdir', '-p', str(self.checkpoint_directory)])

        if self.analysis_type == 'group':

            # create the skeleton JSON content for this isolate
            checkpoint_dict = {
                'initialization': None,
                'reference_processing': None,
                'new_sample_processing': None,
                'snippy_core_and_clean': None,
                'recombination_filtering': None,
                'variant_annotation': None,
            }

            # write the skeleton JSON to the checkpoint file
            with open(checkpoint_file, 'w') as chkpnt_file:
                json.dump(checkpoint_dict, chkpnt_file)

            # verify that the JSON file was successfully created
            try:
                assert file_check(file_path=checkpoint_file, file_or_directory='file')
                return checkpoint_file
            except AssertionError:
                quit_with_error(self.logger, 'Error: initialize_checkpoint_file() failed to create the json checkpoint file for ' + self.name + ' in group ' + self.group)


        if self.analysis_type == 'sample':

            # create the skeleton JSON content for this isolate
            checkpoint_dict = {
                'read_file_preprocessing': None,
                'repair_read_order': None,
                'map_reads': None,
                'individual_analysis': None

            }

            # write the skeleton JSON to the checkpoint file
            with open(checkpoint_file, 'w') as chkpnt_file:
                json.dump(checkpoint_dict, chkpnt_file)

            # verify that the JSON file was successfully created
            try:
                assert file_check(file_path=checkpoint_file, file_or_directory='file')
                return checkpoint_file
            except AssertionError:
                quit_with_error(self.logger, 'Error: initialize_checkpoint_file() failed to create the json checkpoint file for ' + self.name + ' in group ' + self.group)

        if self.analysis_type == 'reference':

            # create the skeleton JSON content for this isolate
            checkpoint_dict = {
                'preprocess_assembly': None,
                'read_file_preprocessing': None,
                'repair_read_order': None,
                'map_reads': None,
                'sort_index_bam_file': None,
                'polish_assembly': None,
                'concatenate_assembly': None,
                'prokka_annotation': None,
                'individual_analysis': None
            }

            # write the skeleton JSON to the checkpoint file
            with open(checkpoint_file, 'w') as chkpnt_file:
                json.dump(checkpoint_dict, chkpnt_file)

            # verify that the JSON file was successfully created
            try:
                assert file_check(file_path=checkpoint_file, file_or_directory='file')
                return checkpoint_file
            except AssertionError:
                quit_with_error(self.logger, 'failed to create the json checkpoint file for ' + self.name + ' in group ' + self.group)

    def read_checkpoint_file(self):
        # check to see if the checkpoint file exists
        try:
            assert file_check(file_path=self.checkpoint_file, file_or_directory='file')
        except AssertionError:
            quit_with_error(self.logger, 'Checkpoint file for ' + self.name + ' in group ' + self.group + ' cannot be located!')
            raise

        # attempt to read in the checkpoint file JSON
        with open(self.checkpoint_file, 'r') as chkpnt_file:
            checkpoint_data = json.load(chkpnt_file)

        # parse checkpoint_data to update the checkpoint statuses
        setattr(self, 'checkpoint_dict', checkpoint_data)

    def update_checkpoint_file(self, analysis_step=None, new_status=None):
        # Validate passed kwargs
        try:
            assert analysis_step is not None
        except AssertionError:
            quit_with_error(self.logger, 'update_checkpoint_file() requires an analysis step be specified')

        try:
            assert analysis_step in list(self.checkpoint_dict.keys())
        except AssertionError:
            quit_with_error(self.logger,
                            'update_checkpoint_file() was passed an invalid analysis step: ' + analysis_step)

        try:
            assert new_status is not None
        except AssertionError:
            quit_with_error(self.logger, 'update_checkpoint_file() requires a new status be specified')

        try:
            assert new_status in [
                None,
                'complete',
                'failed'
            ]
        except AssertionError:
            quit_with_error(self.logger,
                            'update_checkpoint_file() was passed an invalid new status: ' + new_status)

        try:
            assert file_check(file_path=self.checkpoint_file, file_or_directory='file')
        except AssertionError:
            quit_with_error(self.logger, 'file for ' + self.name + ' in group ' + self.group + ' cannot be located!')

        # update the sample object status
        checkpoint_dict = getattr(self, 'checkpoint_dict')
        checkpoint_dict[analysis_step] = new_status
        setattr(self, 'checkpoint_dict', checkpoint_dict)

        # attempt to read in the checkpoint file JSON
        with open(self.checkpoint_file, 'r') as chkpnt_file:
            checkpoint_data = json.load(chkpnt_file)

        # update the JSON entry for analysis step by setting it to new_status
        checkpoint_data[analysis_step] = new_status

        # write the new JSON information to the file
        with open(self.checkpoint_file, 'w') as chkpnt_file:
            json.dump(checkpoint_data, chkpnt_file)
