#!/usr/bin/env python
# encoding: utf-8

import argparse

import mrsnp_plus
from mrsnp_plus.common import log
from mrsnp_plus.common.miscellaneous import ascii_art
import mrsnp_plus.testing.tests


def parse_arguments(__author__, __version__):
    """
    capture user instructions from CLI. What options are available is defined by the passed kwarg entrypoint
    """

    ascii_art(logo='mrsnp', author=__author__, version=__version__)

    ####################################################################################################################
    #   Begin parser definition
    ####################################################################################################################

    parser = argparse.ArgumentParser(prog='mrsnp-plus',
                                     usage='mrsnp-plus <output directory> <input file> [options]',
                                     description=log.bold_yellow('MRSN Single Nucleotide Polymorphism (SNP) Analysis and Variant Annotation Tool\n'),
                                     )

    subparsers = parser.add_subparsers(title='Available commands', help='', metavar='')

    ####################################################################################################################
    #   run
    ####################################################################################################################
    subparser_run = subparsers.add_parser('run',
                                          help='Run mrsnp-plus analysis based on the template provided in the input file',
                                          usage='mrsnp run [options] <output_directory> <reads_directory> <assemblies directory> <input_file>',
                                          description=log.bold_yellow('command "run" validates and performs SNP analysis based on the input file'))

    subparser_run.add_argument('output', type=str, help='path to the directory where output is/will be stored')
    subparser_run.add_argument('reads', type=str, help='path to the directory where the reads files are stored')
    subparser_run.add_argument('assemblies', type=str, help='path to the directory where the assembly(contig) files are stored')
    subparser_run.add_argument('input_file', type=str, help='path to the input file that describes the analysis to be run')

    optional_run_args = subparser_run.add_argument_group('Optional Arguments')
    optional_run_args.add_argument('--trim', type=int, default=50, help='bp to be trimmed from either end of contigs in the reference assembly [50]')
    optional_run_args.add_argument('--filter', type=int, default=500, help='length in bp of the shortest contig in the reference assembly to be included in the analysis [500]')
    optional_run_args.add_argument('--cores', type=int, default=1, help='The number of system cores to use in the analysis')
    optional_run_args.add_argument('--no-gubbins', action='store_true', help='Skip gubbins recombination filtering')
    optional_run_args.add_argument('--keep', type=int, choices=[1, 2], default=2, help='Level of intermediate files to be retained after the run completes, 0 is the least and 2 is the most [2]')
    optional_run_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2], help='the level of reporting done to the terminal window [1]')

    subparser_run.set_defaults(func=mrsnp_plus.run.run)
    subparser_run.set_defaults(entry_point='run')

    ####################################################################################################################
    #   check
    ####################################################################################################################
    subparser_check = subparsers.add_parser('check',
                                          help='Perform validation tests to determine if the analysis described in the input file can be performed',
                                          usage='mrsnp check [options] <output_directory> <reads_directory> <assemblies directory> <input_file>',
                                          description=log.bold_yellow('command "check" will determine if the analysis '
                                                                      'in input file can be performed with the '
                                                                      'information provided. It will validate the '
                                                                      'format of the input file, then scan the '
                                                                      'input/output directories to determine if the '
                                                                      'requisite input files are present or if the '
                                                                      'analysis has already been completed. It will '
                                                                      'also scan your environment for the required '
                                                                      'dependency programs. Finally, it will present '
                                                                      'and log a summary based on the results of '
                                                                      'these tests. CHECK WILL NOT RUN THE ACTUAL ANALYSIS'))

    subparser_check.add_argument('output', type=str, help='path to the directory where output is/will be stored')
    subparser_check.add_argument('reads', type=str, help='path to the directory where the reads files are stored')
    subparser_check.add_argument('assemblies', type=str, help='path to the directory where the assembly(contig) files are stored')
    subparser_check.add_argument('input_file', type=str, help='path to the input file that describes the analysis to be run')

    optional_check_args = subparser_check.add_argument_group('Optional Arguments')
    optional_check_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2, 3], help='the level of reporting done to the terminal window [1]')

    subparser_check.set_defaults(func=mrsnp_plus.run.check)
    subparser_check.set_defaults(entry_point='check')
    subparser_check.set_defaults(trim=50)
    subparser_check.set_defaults(filter=500)
    subparser_check.set_defaults(keep=2)
    subparser_check.set_defaults(no_gubbins=False)

    # TODO add in test methods

    ####################################################################################################################
    #   test
    ####################################################################################################################
    subparser_test = subparsers.add_parser('test',
                                              help='run test set for mrsnp-plus and exit',
                                              usage='mrsnp-plus test <command>',
                                              description=log.bold_yellow('run test data for the selected mrsnp command'))

    subparser_test.add_argument('command', type=str, choices=['input_file', 'check', 'reference', 'samples', 'variant'], help='command to test')
    subparser_test.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2], help='the level of reporting done to the terminal window [1]')
    subparser_test.set_defaults(func=mrsnp_plus.testing.tests.run_tests)


    ####################################################################################################################
    #   version
    ####################################################################################################################
    subparser_version = subparsers.add_parser('version',
                                              help='Get versions and exit',
                                              usage='mrsnp-plus version',
                                              description='returns the version of the installed mrsnp-plus package')

    subparser_version.set_defaults(func=mrsnp_plus.common.miscellaneous.get_version)
    subparser_version.set_defaults(version=__version__)

    ####################################################################################################################
    #
    # Collect args from parser
    #
    ####################################################################################################################

    args = parser.parse_args()

    # turn on debug messaging if verbosity == 2
    if hasattr(args, 'verbosity'):
        if getattr(args, 'verbosity') == 2:
            setattr(args, 'debug', True)
        else:
            setattr(args, 'debug', False)

    # launch analysis
    if hasattr(args, 'func'):
        args.func(args)
    else:
        parser.print_help()