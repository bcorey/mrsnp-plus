#!/usr/bin/env python3
# encoding: utf-8
import csv
import sys

import pandas as pd

from mrsnp_plus.common import log
from mrsnp_plus.common.input_parsing import parse_arguments
from mrsnp_plus.common.miscellaneous import path_clean, file_check, quit_with_error, file_extension_check
from mrsnp_plus.analysis.precheck import analyses_precheck

__author__ = 'Brendan Corey'
__version__ = '1.0.3'



def mrsnp_plus():
    parse_arguments(__author__, __version__)


def run(args):

    groups = analyses_precheck(args=args)

    for group in groups:

        group.initialize_reference()

        group.process_reference()

        group.clean_up_reference()

        group.initialize_new_samples()

        group.process_new_samples()

        group.variant_annotation()

        group.clean_up_samples()


def check(args):

    groups = analyses_precheck(args=args)

    for group in groups:

        group.initialize_reference()

        group.initialize_new_samples()

        group.analysis_preview()



