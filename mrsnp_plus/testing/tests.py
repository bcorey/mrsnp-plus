#!/usr/bin/env python3
# encoding: utf-8
import math
import os
import subprocess
from pathlib import Path
import fnmatch

from psutil import virtual_memory

import mrsnp_plus.testing as testing
from mrsnp_plus.analysis.precheck import analyses_precheck
from mrsnp_plus.analysis.vcf_methods import annotation_summary


def run_tests(args):

    if args.command == 'input_file':
        test_input_file_reader(args)

    if args.command == 'check':
        test_check(args)

    if args.command == 'reference':
        test_reference(args)

    if args.command == 'samples':
        test_samples(args)

    if args.command == 'variant':
        test_variant_analysis(args)

    if args.command == 'summary':
        test_summarize_annotation()


def test_summarize_annotation():

    annotation_table_file = Path(__file__).parents[3] / 'Testing' / 'mrsnp-plus' / 'MRSN702383.annotated_variants.tab'

    group = 'MRSN702383'

    samples = ['MRSN702383', 'MRSN702391', 'MRSN702405', 'MRSN702489', 'MRSN702527']

    annotation_summary(annotation_table_file, samples, group)


def test_variant_analysis(args):

    groups = test_samples(args)

    for group in groups:

        group.variant_annotation()




def test_samples(args):

    groups = test_reference(args)

    for group in groups:
        print(group.name)

        print('\nprocessing samples')

        group.process_new_samples()

    return groups


def test_reference(args):

    args.output = Path(__file__).parents[3] / 'Testing' / 'mrsnp-plus' / 'test_output'
    args.reads = Path(__file__).parents[3] / 'Resources' / 'mrsnp-plus' / 'reads' / 'raw_reads'
    args.assemblies = Path(__file__).parents[3] / 'Resources' / 'mrsnp-plus' / 'assembly_files'
    args.input_file = Path(__file__).parents[3] / 'Resources' / 'mrsnp-plus' / 'input_files' / 'O202_mrsnp_input_file.csv'
    args.entry_point = 'check'
    args.cores = 48

    print('\ninitializing groups')

    groups = analyses_precheck(args=args)

    print(groups)

    for group in groups:
        print(group.name)

        print('\ninitializing reference')

        group.initialize_reference()

        print(group.reference.name)

        print('\ninitializing samples')

        group.initialize_new_samples()

        print('\npreparing reference')

        group.process_reference()

    return groups

def test_check(args):

    input_file_test_list = [
        Path(__file__).parent.parent / 'resources' / 'test_resources' / 'input_files' / 'O202_mrsnp_input_file.csv',
        Path(__file__).parent.parent / 'resources' / 'test_resources' / 'input_files' / 'O202_mrsnp_input_file_blank_reference_and_annotation.csv',
        Path(__file__).parent.parent / 'resources' / 'test_resources' / 'input_files' / 'O202_mrsnp_input_file_two_groups.csv'
    ]

    for input_file in input_file_test_list:
        args.output = Path(__file__).parent.parent / 'testing' / 'test_output'
        args.reads = Path(__file__).parent.parent / 'resources' / 'test_resources' / 'reads'
        args.assemblies = Path(__file__).parent.parent / 'resources' / 'test_resources' / 'assembly'
        args.input_file = input_file
        args.entry_point = 'check'
        args.cores = 4

        print('\ninitializing groups')

        groups = analyses_precheck(args=args)

        print(groups)

        for group in groups:

            print(group.name)

            print('\ninitializing reference')

            group.initialize_reference()

            print(group.reference.name)

            print('\ninitializing samples')

            group.initialize_new_samples()

            group.analysis_preview()

            print('pass')

            #subprocess.run(['rm', '-r', str(args.output)])




def test_input_file_reader(args):

    input_file_test_list = [
        Path(__file__).parent.parent / 'resources' / 'test_resources' / 'input_files' / 'O202_mrsnp_input_file.csv',
        Path(__file__).parent.parent / 'resources' / 'test_resources' / 'input_files' / 'O202_mrsnp_input_file_blank_reference_and_annotation.csv',
        #Path(__file__).parent.parent / 'resources' / 'test_resources' / 'input_files' / 'O202_mrsnp_input_file_missing_one_group.csv',
        #Path(__file__).parent.parent / 'resources' / 'test_resources' / 'input_files' / 'O202_mrsnp_input_file_missing_one_sample.csv',
        Path(__file__).parent.parent / 'resources' / 'test_resources' / 'input_files' / 'O202_mrsnp_input_file_two_groups.csv'
    ]

    for input_file in input_file_test_list:

        args.output = Path(__file__).parent.parent / 'testing' / 'test_output'
        args.reads = Path(__file__).parent.parent / 'resources' / 'test_resources' / 'reads'
        args.assemblies = Path(__file__).parent.parent / 'resources' / 'test_resources' / 'assembly'
        args.input_file = input_file
        args.entry_point = 'check'

        analyses_precheck(args=args)

def test_find_conda_env():

    print(os.environ['CONDA_DEFAULT_ENV'])
    print(os.environ['CONDA_PREFIX'])

    for element in os.walk(Path(os.environ['CONDA_PREFIX']) / 'share'):
        if 'pilon' in element[0]:
            for file in element[2]:
                if file.endswith('.jar'):
                    print(file)

    print(virtual_memory().free)
    print(virtual_memory().free / 100000000)
    print((virtual_memory().free / 100000000) * 0.8)
    print(math.floor((virtual_memory().free / 100000000) * 0.8))