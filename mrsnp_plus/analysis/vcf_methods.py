#!/usr/bin/env python3
# encoding: utf-8

import subprocess
from collections import OrderedDict
import pandas as pd

from mrsnp_plus.common.miscellaneous import quit_with_error, file_check


####################################################################################################################
#
# index_zip_vcf()
#   index and compress vcf file
#
####################################################################################################################
def index_zip_vcf(vcf_file, output_directory, group, logger):
    # generate the indexed and compressed file paths
    compressed_indexed_vcf_file = output_directory / (vcf_file.name + '.gz')

    # generate the command line arguments as a string for logging
    index_zip_cmd = 'bgzip {}; tabix -p vcf {}'.format(str(vcf_file), str(compressed_indexed_vcf_file))

    # call bgzip on the vcf file
    with open(compressed_indexed_vcf_file, 'w') as f:
        zip_process = subprocess.run(
            [
                'bgzip', str(vcf_file),
                '-c'
            ],
            stdout=f,
            stderr=subprocess.PIPE
        )

    # check the return code from index_zip
    try:
        assert zip_process.returncode == 0
    except AssertionError:
        quit_with_error(logger, 'bgzip returned a non-zero returncode for ' + group + ': ' + str(
            zip_process.returncode) + ' : ' + str(zip_process.stderr))

    try:
        assert file_check(file_path=compressed_indexed_vcf_file, file_or_directory='file')
    except AssertionError:
        quit_with_error(logger,
                        'something went wrong during compresseing of the vcf file. bgzip failed to generate the output file for ' + group + ' at the expected path ' + str(
                            compressed_indexed_vcf_file))

    # call tabix on the vcf file
    index_process = subprocess.run(
        [
            'tabix',
            '-p', 'vcf',
            str(compressed_indexed_vcf_file)
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )

    # check the return code from index_zip
    try:
        assert index_process.returncode == 0
    except AssertionError:
        quit_with_error(logger, 'tabix returned a non-zero returncode for ' + group + ': ' + str(
            index_process.returncode))

    try:
        assert file_check(file_path=compressed_indexed_vcf_file, file_or_directory='file')
        return compressed_indexed_vcf_file
    except AssertionError:
        quit_with_error(logger,
                        'something went wrong during indexing of the vcf file. tabix failed to generate the output file for ' + group + ' at the expected path ' + str(
                            compressed_indexed_vcf_file))


####################################################################################################################
#
# merge_vcfs()
#   merge vcf files into a multi-vcf
#
####################################################################################################################
def merge_vcf(vcf_files, output_directory, group, logger):
    logger.log_section_header('Merging vcf files')

    # generate the multi-vcf file path
    multi_vcf_file = output_directory / (group + '.multi.vcf')

    # generate the list of vcf files as a string
    vcf_file_string = ' '.join([str(vcf_file) for vcf_file in vcf_files])

    # generate the command line arguments as a string for logging
    multi_vcf_cmd = 'vcf-merge {} > {}'.format(vcf_file_string, str(multi_vcf_file))

    logger.log('Generating multi-vcf file with the following: \n' + multi_vcf_cmd, verbosity=3)

    # call vcf-merge on the vcf files
    with open(multi_vcf_file, 'w') as f:
        multi_vcf_process = subprocess.run(
            [
                'vcf-merge ' + vcf_file_string,
            ],
            stdout=f,
            stderr=subprocess.PIPE,
            shell=True
        )

    # check the return code from index_zip
    try:
        assert multi_vcf_process.returncode == 0
    except AssertionError:
        print('\n' + str(multi_vcf_process.stderr, 'utf-8'))
        quit_with_error(logger, 'multi_vcf returned a non-zero returncode for ' + group + ': ' + str(
            multi_vcf_process.returncode))

    try:
        assert file_check(file_path=multi_vcf_file, file_or_directory='file')
        return multi_vcf_file
    except AssertionError:
        quit_with_error(logger,
                        'something went wrong during generation of the multi-vcf file. vcf-merge failed to generate the output file for ' + group + ' at the expected path ' + str(
                            multi_vcf_file))


####################################################################################################################
#
# annotate_vcf()
#   import reference annotations from gff file and apply it to vcf file
#
####################################################################################################################
def annotate_vcf(merged_vcf_file, gff_file, output_directory, group, logger):
    logger.log_section_header('Annotating vcf file')

    print(str(gff_file))

    #read in vcf file putting header in output and data into list of lines
    vcf_raw_data_list = []
    vcf_output_list = []
    with open(merged_vcf_file, "r") as f:
        for line in f:
            line = line.strip()
            # copy header to output
            if line.startswith("#"):
                vcf_output_list.append(line)
            else:
                vcf_raw_data_list.append(line)

    #read in gff file ignoring header lines
    gff_list = []
    with open(gff_file, "r") as f:
        for line in f:
            line = line.strip()
            # stop parsing file when sequence is reached
            if line.strip() == "##FASTA":
                break
            # skip header
            if line.startswith("#"):
                continue
            else:
                gff_list.append(line)

    #===============================================================================
                            #GFF parsing/processing
    #===============================================================================
    #Example Data:
    #ID=MRSN4605_00511;Name=yceD_2;db_xref=COG:COG2310;gene=yceD_2;
    #inference=ab initio prediction:Prodigal:2.6,similar to AA sequence:UniProtKB:P80875;
    #locus_tag=MRSN4605_00511;product=General stress protein 16U


    # make gene dictionary with
    #    key: MRSN locus_tag (consistent between the two files) Ex. MRSN4605_00021
    #    value: data fields delimited by tab and ;
    gene_dict = {}
    for raw_data in gff_list:
        # parse MRSN###_#### out of ninth field
        gene_id = raw_data.split("\t")[8].split(";")[0].strip()[3:]
        gene_info_list = raw_data.strip().split("\t")

        if len(gene_info_list) != 9:
            raise ValueError("FATAL ERROR: expected 9 columns in GFF, got " + str(len(gene_info_list)))

        gene_anno_info_list = gene_info_list[-1].split(";")
        gene_info_list = gene_info_list[:-1]

        # should only encounter each locus tag once: Ex. MRSN####_####
        if gene_id in gene_dict:
            logger.log("WARNING! Duplicate entries in GFF for gene_id: " + gene_id)
            logger.log("Overwriting: " + gene_id + "\t" + str(gene_dict[gene_id]))
            logger.log("...with: " + gene_id + "\t" + str(gene_anno_info_list))

        # instantiate "." for all fields, prevents missing values keeps column lengths identical
        gene_dict[gene_id] = [".", #"gene_CHROM",
                              ".", #"prediction_software",
                              ".", #"gene_seq_type",
                              ".", #"gene_coord_start",
                              ".", #"gene_coord_end",
                              ".", #"UNK1",
                              ".", #"Strand",
                              ".", #"UNK2",
                                OrderedDict({
                                  "ID=": ".",
                                  "eC_number=": ".",
                                  "Name=": ".",
                                  "db_xref=": ".",
                                  "gene=": ".",
                                  "inference=": ".",
                                  "locus_tag=": ".",
                                  "note=": ".",
                                  "rpt_family=": ".",
                                  "rpt_type=": ".",
                                  "rpt_unit_seq=": ".",
                                  "product=": ".",
                                })
                                ]
        for annotation in gene_anno_info_list:
            eq_slice_index = annotation.find('=') + 1
            key = annotation[:eq_slice_index]
            value = annotation[eq_slice_index:]
            if key not in gene_dict[gene_id][-1]:
                print(annotation)
                raise ValueError("missed a field! ", key)
            gene_dict[gene_id][-1][key] = value

        for i, field in enumerate(gene_info_list):
            gene_dict[gene_id][i] = field

    #===============================================================================
                            #VCF parsing/processing
    #===============================================================================

    count_false_concatig_variants = 0
    variant_dict = OrderedDict()
    longest = [] #DELETE ME
    for raw_data in vcf_raw_data_list:

        data_fields_list = raw_data.split("\t")

        # removes false variants called on the border of contigs (concatig is padded with N's)
        if "N" in data_fields_list[3]:
            count_false_concatig_variants += 1
            continue

        # unique variant ID made with chromosome, position, REF, and ALT concatenated with "|"
        variant_id = "|".join([data_fields_list[0], data_fields_list[1], data_fields_list[3], data_fields_list[4]])
        #print(variant_id)

        #This is an unexpected error, prevents overwriting dict if true (vcf file is probably messed up)
        if variant_id in variant_dict:
            raise ValueError("Duplicate variant!" + variant_id)

        # [:] to pass by value and not by reference
        variant_dict[variant_id] = data_fields_list[:]
        #instantiates dict for known fields, if key is not found while parsing data, defaults output to "."
        variant_dict[variant_id][7] = OrderedDict({
                                            "AB=": ".", # data before pipes
                                            "AC=": ".",
                                            "AN=": ".",
                                            "ANN=": ".", # pipe delim data removed from this field
                                            "AO=": ".", # data after pipes
                                            "DP=": ".",
                                            "LOF=": ".",
                                            "QA=": ".",
                                            "QR=": ".",
                                            "RO=": ".",
                                            "SF=": ".",
                                            "TYPE=": "."
                                            })

        #parse INFO column of VCF
        for annotation in data_fields_list[7].split(";"):
            eq_slice_index = annotation.find('=') + 1
            if eq_slice_index <= 0:
                raise ValueError("VCF field must contain '=': ", variant_id, annotation)
            key = annotation[:eq_slice_index]
            value = annotation[eq_slice_index:]
            if key not in variant_dict[variant_id][7]:
                raise ValueError("missed a field! ", key, annotation, data_fields_list)
            if key != "ANN=":
                value = value.replace("|", ",") # LOF= field has pipes
            variant_dict[variant_id][7][key] = value

        # split ANN=<nuc> from pipe delimited snipeff annotations
        ANN_val = variant_dict[variant_id][7]["ANN="].split("|")[0]
        # save pipe delimited snipeff annotations to a new list
        snipeff_pipe_list = variant_dict[variant_id][7]["ANN="].split("|")[1:]
        # overwrite ANN= value to <nuc> effectively removing pipe delimited annotations
        variant_dict[variant_id][7]["ANN="] = ANN_val

        # get first gene locus_tag from VCF to pull corresponding gff data (may need to add more delimiters beyond '-' and '&')
        MRSN_locus_tag = snipeff_pipe_list[2].split("-")[0].split("&")[0]

        # check locus_tag was found in gff and that it was parsed correctly
        if MRSN_locus_tag not in gene_dict:
            raise ValueError("FATAL Error: MRSN_locus_tag:", MRSN_locus_tag,
             "not found in gff_file. If multiple locus tags are present a new delimiter may need to be added to annotate_vcf.py")

        # prepare print line for fixed length vcf INFO field input
        vcf_output = []
        for key, value in variant_dict[variant_id][7].items():
            vcf_output.append(key + value)
        vcf_output = ";".join(vcf_output)

        # prepare print line for fixed length gff annotation data
        gff_output = []
        # assign non-variable columns
        gff_output = gene_dict[MRSN_locus_tag][:-1]
        # assign variable columns
        for key, value in gene_dict[MRSN_locus_tag][-1].items():
            gff_output.append(key + value)


        gff_output = ";".join(gff_output)

        # prepare print line for VARIABLE length snipeff data (scraped from input vcf)
        snipeff_output = ";".join(snipeff_pipe_list)

        # overwrite input vcf INFO column with formatted vcf data and gff annotation
        variant_dict[variant_id][7] = "|".join([gff_output, vcf_output, snipeff_output])

    # prepare print statement for each line in vcf output
    for ID, data in variant_dict.items():
        vcf_output_list.append("\t".join(data))

    # summary statements
    logger.log("num output lines: {}".format(str(len(vcf_output_list))))
    logger.log("Removed " + str(count_false_concatig_variants) + " variants called in the padding between contigs. ")
    logger.log(str(len(vcf_raw_data_list)-count_false_concatig_variants) + " variants remain.")

    #write to output
    output_file = output_directory / (group + '.annotated.vcf')
    with open(output_file, "w") as f:
        f.write("\n".join(vcf_output_list))

    return output_file

####################################################################################################################
#
# generate_annotated_vcf_table()
#   convert the annotated multi-vcf file to a table
#
####################################################################################################################
def generate_annotated_vcf_table(annotated_vcf_file, output_directory, group, logger):
    logger.log_section_header('Converting annotated multi-vcf file to a table')
    # read in vcf file putting header in output and data into list of lines
    vcf_raw_data_list = []
    column_headers = []
    with open(annotated_vcf_file, "r") as f:
        for line in f:
            line = line.strip()
            # copy column header
            if line.startswith("#CHROM"):
                column_headers = line.split()
            # discard other header info
            elif line.startswith("#"):
                continue
            else:
                vcf_raw_data_list.append(line)

    # hold each output line as an entry in a list
    table_output_list = []
    for raw_data in vcf_raw_data_list:

        data_fields_list = raw_data.split("\t")

        # annotate_vcf.py filters out variants with N's in REF
        if "N" in data_fields_list[3]:
            quit_with_error(logger, "Run annotate_vcf.py first to filter vcf and add annotations. Found N's in REF column.")

        # parse INFO column of VCF
        info_col_list_1_layer = data_fields_list[7].split("|")
        if len(info_col_list_1_layer) != 3:
            quit_with_error(logger, "INFO list should have three fields separated by '|', got {}".format(len(info_col_list_1_layer)))

        gff_data = info_col_list_1_layer[0].split(";")
        vcf_data = info_col_list_1_layer[1].split(";")
        gff_vcf_data = gff_data + vcf_data

        snip_eff_data = info_col_list_1_layer[2].split(";")
        if len(snip_eff_data) < 15:
            quit_with_error(logger, "Too few snipeff data fields, expected >=15 but got {}".format(snip_eff_data))
        snip_eff_data = snip_eff_data[:15]  # only take meta data for first 15 fields
        # ensure no blank cells in output
        for i, field in enumerate(snip_eff_data):
            if len(field) == 0:
                snip_eff_data[i] = "."

        # prepare print line for output
        output_line = "\t".join(data_fields_list[:7] + gff_vcf_data + snip_eff_data + data_fields_list[8:])
        table_output_list.append(output_line)

    gff_column_headers = ["gene_CHROM", "prediction_software", "gene_seq_type", "gene_coord_start", "gene_coord_end",
                          "UNK1", "Strand", "UNK2", "ID=", "eC_number=", "Name=", "db_xref=", "gene=", "inference=",
                          "locus_tag=", "note=", "rpt_family=", "rpt_type=", "rpt_unit_seq", "product="]

    vcf_column_headers = ["AB=", "AC=", "AN=", "ANN=", "AO=", "DP=", "LOF=", "QA=", "QR=", "RO=", "SF=", "TYPE="]
    gff_vcf_column_headers = gff_column_headers + vcf_column_headers

    snpeff_column_headers = ["Annotation", "Annotation_Impact", "Gene_Name", "Gene_ID", "Feature_Type", "Feature_ID",
                             "Transcript_BioType", "Rank", "HGVS.c", "HGVS.p", "cDNA.pos / cDNA.length",
                             "CDS.pos / CDS.length", "AA.pos / AA.length", "Distance", "ERRORS / WARNINGS / INFO"]

    # form first line of output
    column_header_line = "\t".join(
        column_headers[:7] + gff_vcf_column_headers + snpeff_column_headers + column_headers[8:]) + "\n"

    # check no variants were lost
    if len(vcf_raw_data_list) != len(table_output_list):
        quit_with_error(logger, "Data was lost, went from {} rows to {} rows".format(len(vcf_raw_data_list), len(table_output_list)))

    # check every line has same length and will be parsed correctly using .split("\t") method
    num_cols = len(column_header_line.split("\t"))
    for line in table_output_list:
        if len(line.split("\t")) != num_cols:
            for i in range(0, num_cols-1):
                print(column_header_line.split('\t')[i] + ' ' + line.split('\t')[i])
            print(line.split('\t')[num_cols])
            quit_with_error(logger, "Not every line has same length on a tab split! Offender: {}".format(str(line)))

    # summary statements
    logger.log("num variants: {}".format(str(len(table_output_list))))

    # write to output
    output_file = output_directory / (group + '.annotated_variants.tab')

    with open(output_file, "w") as f:
        f.write(column_header_line)
        f.write("\n".join(table_output_list)[:-1])

    return output_file


####################################################################################################################
#
# annotation_summary()
#   Create a succint summary from the annotated vcf table
#
####################################################################################################################
def annotation_summary(annotation_table_file, samples, group, logger=None):
    logger.log_section_header('Summarizing annotated variants')

    annotation_dataframe = pd.read_csv(annotation_table_file, sep='\t', na_values='.')

    # remove tRNA hits
    annotation_dataframe.drop(annotation_dataframe[annotation_dataframe['gene_seq_type'] == 'tRNA'].index, axis=0, inplace=True)

    # filter out less significant information (columns)
    significant_columns = ['Name=', 'product=', 'locus_tag=', 'TYPE=', 'REF', 'ALT', 'POS', 'Annotation', 'Annotation_Impact']
    for sample in sorted(samples):
        significant_columns.append(sample)
    subset_annotation_dataframe = annotation_dataframe.loc[:, annotation_dataframe.columns.intersection(significant_columns)]

    # clean up the column names
    renaming_map = {
        'Name=': 'Gene',
        'product=': 'Translated Product',
        'locus_tag=': 'Locus tag',
        'TYPE=': 'Variant Type',
        'REF': 'Reference',
        'ALT': 'Variant',
        'POS': 'Position',
        'Annotation_Impact': 'Annotation Impact'
    }
    subset_annotation_dataframe.rename(mapper=renaming_map, axis=1, inplace=True)

    # clean up the column contents
    for column_label in renaming_map.values():
        if subset_annotation_dataframe[column_label].astype('str').str.contains('=').any():
            split_column = subset_annotation_dataframe[column_label].str.split('=', expand=True)
            subset_annotation_dataframe[column_label] = split_column[1]

    # clean up for the annotation column to remove underscores
    subset_annotation_dataframe['Annotation'].replace('_', ' ', regex=True, inplace=True)
    subset_annotation_dataframe['Annotation'].replace('&', ', ', regex=True, inplace=True)

    # for the sample columns, mask "." values to null and all other values to "+"
    for sample in samples:
        subset_annotation_dataframe[sample].mask(subset_annotation_dataframe[sample].isna(), '', inplace=True)
        subset_annotation_dataframe[sample].mask(subset_annotation_dataframe[sample] != '', "+", inplace=True)

    # sort the dataframe by the locus tag column
    subset_annotation_dataframe.sort_values(by='Locus tag', inplace=True)

    pd.set_option("display.max_rows", None, "display.max_columns", None)

    summary_file = annotation_table_file.parent / 'variant_analysis_summary.csv'

    subset_annotation_dataframe.to_csv(summary_file, index=False)

    logger.log('Variant analysis summary table saved to {}'.format(str(summary_file)))





