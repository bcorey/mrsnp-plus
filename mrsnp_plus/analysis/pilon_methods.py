#!/usr/bin/env python3
# encoding: utf-8
import math
import os
import shutil
import subprocess

from psutil import virtual_memory

from Bio import SeqIO

from mrsnp_plus.common import log
from mrsnp_plus.common.miscellaneous import file_check, quit_with_error, path_clean


####################################################################################################################
#
# polish_assembly()
#   use pilon to polish the reference assembly
#
####################################################################################################################
def polish_assembly(assembly_file, sorted_bam_file, output_directory, name, logger, checkpoint, cores=1):
    logger.log_section_header('Polishing assembly using pilon')

    output_file = output_directory / (str(name) + '.pilon_polished_assembly.fasta')
    clean_pilon_output_file = output_file.parent / (name + '.pilon_polished_assembly.cleaned.fasta')

    # check for a pilon output file in the output directory
    if file_check(file_path=clean_pilon_output_file, file_or_directory='file'):
        logger.log('Located a pilon polished assembly file for ' + log.bold_green(str(name)))

        # verify that the file can be trusted using the checkpoint object
        checkpoint.read_checkpoint_file()
        if checkpoint.checkpoint_dict['polish_assembly'] == 'complete':
            logger.log('checkpoint file indicates that this step was completed. Skipping!')
            return clean_pilon_output_file
        else:
            logger.log('checkpoint file does not recognize this step as complete, so the existing output of this step '
                       'will be trashed the analysis redone')
            subprocess.run(['rm', '-r', str(output_directory)])

    # ensure that the assembly_file is accessible at the passed path
    try:
        assert file_check(file_path=assembly_file, file_or_directory='file')
        logger.log('Located the assembly file at ' + log.bold_green(str(assembly_file)))
    except AssertionError:
        quit_with_error(logger,
                        'could not locate the raw reference assembly file at: ' + log.bold_red(str(assembly_file)))

    # ensure that the sorted_bam_file and the associated index file are accessible at the passed path
    try:
        assert file_check(file_path=sorted_bam_file, file_or_directory='file')
        logger.log('Located the sorted bam file at ' + log.bold_green(str(sorted_bam_file)))
    except AssertionError:
        quit_with_error(logger,
                        'could not locate the sorted bam file at: ' + log.bold_red(str(sorted_bam_file)))

    sorted_bam_index_file = sorted_bam_file.parent / (str(name) + '.bam.sorted.bai')

    try:
        assert file_check(file_path=sorted_bam_index_file, file_or_directory='file')
        logger.log('Located the sorted bam index file at ' + log.bold_green(str(sorted_bam_index_file)))
    except AssertionError:
        quit_with_error(logger,
                        'could not locate the sorted bam index file at: ' + log.bold_red(str(sorted_bam_index_file)))

    # check to make sure that the number of cores does not exceed the system available resources
    if cores > len(os.sched_getaffinity(0)):
        quit_with_error(logger,
                        'User requested more cores for this analysis than are currently available.\nThe number of'
                        ' currently available cores is: ' + str(len(os.sched_getaffinity(0))))

    # check to see how much memory is free on the system. We will at MOST allow snippy to get its hands on 80%
    mem_to_use = math.floor((virtual_memory().available / 1000000000) * 0.8)

    # check to make sure that the output_directory exists. If not, make it
    try:
        assert file_check(file_path=output_directory, file_or_directory='directory')
    except AssertionError:
        subprocess.run(['mkdir', str(output_directory)])

    # Since we want to allocate more than 1GB of RAM (probably) to pilon, we need to find the .jar file
    for element in os.walk(path_clean(file_path=os.environ['CONDA_PREFIX']) / 'share'):
        if 'pilon' in element[0]:
            for file in element[2]:
                if file.endswith('.jar'):
                    pilon_jar_file = path_clean(element[0]) / file
                    break

    # write the pilon command to a string for logging
    pilon_cmd = 'java -Xmx{}G -jar {} --genome {} --unpaired {} --changes --vcf --outdir {} --output {}'.format(
        str(mem_to_use), str(pilon_jar_file), str(assembly_file), str(sorted_bam_file), str(output_directory), str(name)
        )

    logger.log('\n Now running pilon with the following command: \n' + log.bold_yellow(pilon_cmd), verbosity=3)

    # run the pilon command using subprocess module
    try:
        pilon_process = subprocess.run(
            [
                'java',
                '-Xmx' + str(mem_to_use) + 'G',
                '-jar', str(pilon_jar_file),
                '--genome', str(assembly_file),
                '--unpaired', str(sorted_bam_file),
                '--changes',
                '--vcf',
                '--outdir', str(output_directory),
                '--output', name
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            timeout=900
        )
    except subprocess.TimeoutExpired:
        snippy_consensus = output_directory.parent / 'snippy_output' / (name + 'consensus.fa')
        logger.log('Pilon timed out (failed to complete after 15 minutes). We will revert to using the snippy conensus file: {}'.format(str(snippy_consensus)))
        return snippy_consensus


    logger.log('\n' + str(pilon_process.stderr, 'utf-8'))

    # check the return code from snippy
    try:
        assert pilon_process.returncode == 0
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='polish_assembly', new_status='failed')
        quit_with_error(logger,
                        'Pilon returned a non-zero returncode for ' + name + ': ' + str(pilon_process.returncode))

    # change the name of the output file to the one we want to use as the output_file
    pilon_fasta_file = output_directory / (name + '.fasta')
    shutil.move(str(pilon_fasta_file), str(output_file))

    # validate that the output file was generated where we were expecting it
    try:
        assert file_check(file_path=output_file, file_or_directory='file')
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='polish_assembly', new_status='failed')
        quit_with_error(logger,
                        'something went wrong during error correction. Pilon failed to generate the corrected assembly file for ' + name + ' at the expected path ' + str(
                            output_file))

    pilon_output_header_correction(output_file, clean_pilon_output_file, logger, checkpoint)
    checkpoint.update_checkpoint_file(analysis_step='polish_assembly', new_status='complete')

    report_pilon_changes(output_directory, name, logger)

    return clean_pilon_output_file


def pilon_output_header_correction(pilon_output_file, clean_pilon_output_file, logger, checkpoint):

    for seq_record in SeqIO.parse(str(pilon_output_file), "fasta"):
        seq_length = len(seq_record.seq)
        seq_record.id = '_'.join(seq_record.id.split('_')[:-1]) + '\t' + str(seq_length) + ' bp'
        seq_record.description = ''

        with open(clean_pilon_output_file, 'a') as clean_assembly:
            SeqIO.write(seq_record, clean_assembly, 'fasta')

def report_pilon_changes(output_directory, name, logger):
    pilon_changes_file = output_directory / (name + '.changes')

    with open(pilon_changes_file, 'r') as f:

        changes = f.readlines()

        logger.log('Pilon made {} changes to the reference'.format(str(len(changes))))

        for change in changes:
            logger.log(change.strip())
