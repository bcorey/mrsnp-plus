#!/usr/bin/env python3
# encoding: utf-8
import datetime
import os
import subprocess
from collections import OrderedDict
from Bio import SeqIO




####################################################################################################################
#
# bowtie2()
#   call to bowtie2 to align the processed reads for the specified sample to the designated reference
#
####################################################################################################################
from mrsnp_plus.common import log
from mrsnp_plus.common.miscellaneous import file_check, path_clean


def bowtie2(sample):
    sample.sample_logger.log_section_header('Using bowtie2 to align sample reads to reference')

    # use the checkpoint file to see if this step has already been completed
    sample.read_checkpoint_file()
    if sample.bowtie2_status == 'complete':
        sample.sample_logger.log(
            'Checkpoint file indicates that bowtie2 has already successfully been completed, so skipping this step!')
        return

    # if the checkpoint file isn't marked complete, we need to shred any existing output and start over
    if file_check(file_path=sample.sam_file, file_or_directory='file'):
        subprocess.run(['rm', str(sample.sam_file)])

    # verify that we have all of the requisite input files for this step
    try:
        assert file_check(file_path=sample.r1_fastq_sorted, file_or_directory='file') and file_check(
            file_path=sample.r2_fastq_sorted, file_or_directory='file')
    except AssertionError:
        sample.sample_logger.log(log.bold_red('ERROR: '))

    sample.sample_logger.log("Now running bowtie2 for: " + sample.sample_name + " against: " + sample.reference)
    sample.sample_logger.log("The sorted R1 file for this sample: " + str(sample.r1_fastq_sorted))
    sample.sample_logger.log("The sorted R2 file for this sample: " + str(sample.r2_fastq_sorted))
    sample.sample_logger.log("The unpaired reads file for this sample: " + str(sample.unpaired_fastq))
    sample.sample_logger.log("The index file path for this sample: " + str(sample.group_directory) + sample.reference)

    # we will run bowtie2 in one of two ways depending on whether or not repair.sh (multiprocessing_pesort) placed any
    # reads in the unpaired fastq file

    if os.stat(sample.unpaired_fastq).st_size != 0:

        bowtie2_process = subprocess.run(["bowtie2",
                                          "--local",
                                          "-X", "1500",
                                          "--mm",
                                          "-x",
                                          str(sample.group_directory / 'reference' / sample.reference),
                                          "-1", str(sample.r1_fastq_sorted),
                                          "-2", str(sample.r2_fastq_sorted),
                                          "-U", str(sample.unpaired_fastq),
                                          "-S", str(sample.sam_file),
                                          "--threads", str(sample.cores_per_thread)],
                                         stderr=subprocess.PIPE
                                         )

    else:

        sample.sample_logger.log("\nThe unpaired reads file for this sample, " + str(
            sample.unpaired_fastq) + " was empty and will be omitted from the analysis")

        bowtie2_process = subprocess.run(["bowtie2",
                                          "--local",
                                          "-X",
                                          "1500",
                                          "--mm",
                                          "-x",
                                          str(sample.group_directory / 'reference' / sample.reference),
                                          "-1", str(sample.r1_fastq_sorted),
                                          "-2", str(sample.r2_fastq_sorted),
                                          "-S", str(sample.sam_file),
                                          "--threads", str(sample.cores_per_thread)],
                                         stderr=subprocess.PIPE
                                         )

    sample.sample_logger.log('\n' + str(bowtie2_process.stderr, 'utf-8'))

    # check the return code from bowtie2
    try:
        assert bowtie2_process.returncode == 0
    except AssertionError:
        sample.sample_logger.log(log.bold_red(
            'ERROR: bowtie2 returned a non-zero returncode for ' + sample.sample_name + ': ' + str(
                bowtie2_process.returncode)))
        sample.update_checkpoint_file(analysis_step='bowtie2_status', new_status='failed')
        raise

    # update the checkpoint file to reflect the status
    sample.update_checkpoint_file(analysis_step='bowtie2_status', new_status='complete')


####################################################################################################################
#
# sam_to_bam()
#   convert the .sam file output from bowtie2 to a .bam file
#
####################################################################################################################
def sam_to_bam(sample):
    sample.sample_logger.log_section_header(
        'Converting the .sam file output from bowtie2 to a .bam file using Samtools')

    # use the checkpoint file to see if this step has already been completed
    sample.read_checkpoint_file()
    if sample.sam_to_bam_status == 'complete':
        sample.sample_logger.log(
            'Checkpoint file indicates that sam_to_bam() has already successfully been completed, so skipping this step!')
        return

    # if the checkpoint file isn't marked complete, we need to shred any existing output and start over
    if file_check(file_path=sample.bam_file, file_or_directory='file'):
        subprocess.run(['rm', str(sample.bam_file)])

    # convert the .sam file output from bowtie2 to a .bam file
    with open(sample.bam_file, 'w') as sample_bam_file:
        convert_to_sam_process = subprocess.run(
            ["samtools",
             "view",
             "-b",
             str(sample.sam_file)],
            stdout=sample_bam_file,
            stderr=subprocess.PIPE,
            universal_newlines=True
        )

    sample.sample_logger.log(str(convert_to_sam_process.stderr))

    # check the return code from samtools
    try:
        assert convert_to_sam_process.returncode == 0
    except AssertionError:
        sample.sample_logger.log(
            log.bold_red('ERROR: samtools returned a non-zero returncode for ' + sample.sample_name))
        sample.update_checkpoint_file(analysis_step='sam_to_bam_status', new_status='failed')
        raise

    # update the checkpoint file to reflect the status
    sample.update_checkpoint_file(analysis_step='sam_to_bam_status', new_status='complete')


####################################################################################################################
#
# sort_bam()
#   sort the newly created .bam file
#
####################################################################################################################
def sort_bam(sample):
    sample.sample_logger.log_section_header('Using samtools to sort the .bam file')

    # use the checkpoint file to see if this step has already been completed
    sample.read_checkpoint_file()
    if sample.sort_bam_status == 'complete':
        sample.sample_logger.log(
            'Checkpoint file indicates that sort_bam() has already successfully been completed, so skipping this step!')
        return

    # if the checkpoint file isn't marked complete, we need to shred any existing output and start over
    if file_check(file_path=sample.sorted_bam_file, file_or_directory='file'):
        subprocess.run(['rm', str(sample.sorted_bam_file)])

    # sort the newly created .bam file
    with open(sample.sorted_bam_file, 'w') as sample_sorted_bam_file:
        sort_bam_process = subprocess.run(
            ["samtools",
             "sort",
             str(sample.bam_file)],
            stdout=sample_sorted_bam_file,
            stderr=subprocess.PIPE,
            universal_newlines=True
        )

    sample.sample_logger.log(str(sort_bam_process.stderr))

    # check the return code from samtools
    try:
        assert sort_bam_process.returncode == 0
    except AssertionError:
        sample.sample_logger.log(
            log.bold_red('ERROR: samtools returned a non-zero returncode for ' + sample.sample_name))
        sample.update_checkpoint_file(analysis_step='sort_bam_status', new_status='failed')
        raise

    # update the checkpoint file to reflect the status
    sample.update_checkpoint_file(analysis_step='sort_bam_status', new_status='complete')


####################################################################################################################
#
# generate_vcf_file()
#   run mpileup on the sorted .bam file
#
####################################################################################################################
def generate_vcf_file(sample):
    sample.sample_logger.log_section_header('Using bcftools mpileup function to generate vcf file from sorted bam file')

    # use the checkpoint file to see if this step has already been completed
    sample.read_checkpoint_file()
    if sample.generate_vcf_status == 'complete':
        sample.sample_logger.log(
            'Checkpoint file indicates that generate_vcf() has already successfully been completed, so skipping this step!')
        return

    # if the checkpoint file isn't marked complete, we need to shred any existing output and start over
    if file_check(file_path=sample.vcf_file, file_or_directory='file'):
        subprocess.run(['rm', str(sample.vcf_file)])

    # ensure that the reference assembly concatig file is available
    try:
        file_check(file_path=sample.reference_assembly_concatig_file, file_or_directory='file')
    except AssertionError:
        sample.sample_logger.log(
            log.bold_red('ERROR: generate_vcf_file() failed to locate the reference assembly concatig file ' + sample.reference_assembly_concatig_file))
        sample.update_checkpoint_file(analysis_step='generate_vcf_status', new_status='failed')
        raise

    # run mpileup on the sorted .bam file and write the output to the sample.vcf_file (.bcf)
    with open(sample.vcf_file, 'w') as sample_vcf_file:
        generate_vcf_process = subprocess.run(
            ["bcftools",
             "mpileup",
             "--output-type", "z",
             "-f",
             str(sample.reference_assembly_concatig_file),
             str(sample.sorted_bam_file)],
            stdout=sample_vcf_file,
            stderr=subprocess.PIPE,
            universal_newlines=True
        )

    sample.sample_logger.log(str(generate_vcf_process.stderr))

    # check the return code from bcftools
    try:
        assert generate_vcf_process.returncode == 0
    except AssertionError:
        sample.sample_logger.log(
            log.bold_red('ERROR: bcftools returned a non-zero returncode for ' + sample.sample_name))
        sample.update_checkpoint_file(analysis_step='generate_vcf_status', new_status='failed')
        raise

    # update the checkpoint file to reflect the status
    sample.update_checkpoint_file(analysis_step='generate_vcf_status', new_status='complete')


####################################################################################################################
#
# call_variants()
#   perform variant calling on the processed .bam file and output as a .bcf file
#
####################################################################################################################
def call_variants(sample):
    sample.sample_logger.log_section_header('Using bcftools call function to call variants on vcf file')

    # use the checkpoint file to see if this step has already been completed
    sample.read_checkpoint_file()
    if sample.call_variants_status == 'complete':
        sample.sample_logger.log(
            'Checkpoint file indicates that call_variants() has already successfully been completed, so skipping this step!')
        return

    # if the checkpoint file isn't marked complete, we need to shred any existing output and start over
    if file_check(file_path=sample.call_vcf_file, file_or_directory='file'):
        subprocess.run(['rm', str(sample.call_vcf_file)])

    # perform variant calling on the processed .bam file and write the output to the sample.call_vcf_file (.call.bcf)
    with open(sample.call_vcf_file, 'w') as sample_call_vcf_file:
        call_variants_process = subprocess.run(
            ["bcftools",
             "call",
             "--output-type", "z",
             "-cv",
             str(sample.vcf_file)],
            stdout=sample_call_vcf_file,
            stderr=subprocess.PIPE,
            universal_newlines=True
        )

    sample.sample_logger.log(str(call_variants_process.stderr))

    # check the return code from bcftools
    try:
        assert call_variants_process.returncode == 0
    except AssertionError:
        sample.sample_logger.log(
            log.bold_red('ERROR: bcftools returned a non-zero returncode for ' + sample.sample_name))
        sample.update_checkpoint_file(analysis_step='call_variants_status', new_status='failed')
        raise

    # update the checkpoint file to reflect the status
    sample.update_checkpoint_file(analysis_step='call_variants_status', new_status='complete')


####################################################################################################################
#
# apply_filters()
#   filter the variant calls in the .bcf file that are within 1 base of an indel
#   filter the variant calls for quality/depth and observed SNP frequency in read alignment
#   output as compressed .vcf.gz
#
####################################################################################################################
def apply_filters(sample):
    sample.sample_logger.log_section_header('Using bcftools filter function to perform two rounds of variant filtering')
    sample.sample_logger.log_explanation('Filter 1: Exclusion of INDELs')
    sample.sample_logger.log_explanation(
        'Filter 2: Remove low coverage/quality SNP calls, as well as those at lower than 80% frequency in the read alignment')

    # use the checkpoint file to see if this step has already been completed
    sample.read_checkpoint_file()
    if sample.apply_filters_status == 'complete':
        sample.sample_logger.log(
            'Checkpoint file indicates that apply_filters() has already successfully been completed, so skipping this step!')
        return

    # if the checkpoint file isn't marked complete, we need to shred any existing output and start over
    if file_check(file_path=sample.filter1_vcf_file, file_or_directory='file'):
        subprocess.run(['rm', str(sample.filter1_vcf_file)])
    if file_check(file_path=sample.filter2_vcf_file, file_or_directory='file'):
        subprocess.run(['rm', str(sample.filter2_vcf_file)])

    # filter the variant calls in the .bcf file that are within 1 base of an indel and write the output to the
    # sample.filter1_vcf_file (.filter1.bcf)
    with open(sample.filter1_vcf_file, 'w') as sample_filter1_vcf_file:
        filter_1_process = subprocess.run(
            ["bcftools",
             "filter",
             "--exclude",
             "INDEL=1",
             "--output-type", "z",
             str(sample.call_vcf_file)],
            stdout=sample_filter1_vcf_file,
            stderr=subprocess.PIPE,
            universal_newlines=True
        )

    sample.sample_logger.log(str(filter_1_process.stderr))

    # check the return code from bcftools
    try:
        assert filter_1_process.returncode == 0
    except AssertionError:
        sample.sample_logger.log(
            log.bold_red('ERROR: bcftools returned a non-zero returncode during filter1 for ' + sample.sample_name))
        sample.update_checkpoint_file(analysis_step='apply_filters_status', new_status='failed')
        raise

    sample.sample_logger.log('\n Filter 1 is complete')

    # filter the variant calls in the indel filtered .bcf file for quality and frequency and write the output to the
    # sample.filter2_vcf_file (.filter2.bcf.gz)
    with open(sample.filter2_vcf_file, 'w') as sample_filter2_vcf_file:
        filter_2_process = subprocess.run(
            ["bcftools",
             "filter",
             "--include",
             "QUAL>=100 && ((DP4[2]+DP4[3]) >= 10) && ((1-((DP4[0]+DP4[1])/(DP4[2]+DP4[3]))) >= 0.8)",
             "--output-type", "z",
             str(sample.filter1_vcf_file)],
            stdout=sample_filter2_vcf_file,
            stderr=subprocess.PIPE,
            universal_newlines=True
        )

    sample.sample_logger.log(str(filter_2_process.stderr))
    sample.sample_logger.log('\n Filter 2 is complete')
    sample.sample_logger.log('\n Step complete')

    # check the return code from bcftools
    try:
        assert filter_2_process.returncode == 0
    except AssertionError:
        sample.sample_logger.log(
            log.bold_red('ERROR: bcftools returned a non-zero returncode during filter2 for ' + sample.sample_name))
        sample.update_checkpoint_file(analysis_step='apply_filters_status', new_status='failed')
        raise

    # update the checkpoint file to reflect the status
    sample.update_checkpoint_file(analysis_step='apply_filters_status', new_status='complete')


####################################################################################################################
#
# index_variant_calls()
#   index the filtered variant calls
#
####################################################################################################################
def index_variant_calls(sample):
    sample.sample_logger.log_section_header('Indexing filtered variant calls using bcftools index')

    # use the checkpoint file to see if this step has already been completed
    sample.read_checkpoint_file()
    if sample.index_variant_calls_status == 'complete':
        sample.sample_logger.log(
            'Checkpoint file indicates that index_variant_calls() has already successfully been completed, so skipping this step!')
        return

    # if the checkpoint file isn't marked complete, we need to shred any existing output and start over. This step is
    # further complicated as it acts on the output file of apply_filters() as opposed to generating its own output.
    # Since vcf filtering is relatively low cost computationally, we'll just roll the checkpoint file back to
    # pre-apply_filters() and call that method again.
    sample.update_checkpoint_file(analysis_step='apply_filters_status', new_status='failed')
    apply_filters(sample)

    # index the filtered variant calls
    index_variant_calls_process = subprocess.run(
        ["bcftools",
         "index",
         str(sample.filter2_vcf_file)],
        stderr=subprocess.PIPE,
        universal_newlines=True
    )
    sample.sample_logger.log(str(index_variant_calls_process.stderr))

    # check the return code from bcftools
    try:
        assert index_variant_calls_process.returncode == 0
    except AssertionError:
        sample.sample_logger.log(
            log.bold_red('ERROR: bcftools returned a non-zero returncode for ' + sample.sample_name))
        sample.update_checkpoint_file(analysis_step='index_variant_calls_status', new_status='failed')
        raise

    # update the checkpoint file to reflect the status
    sample.update_checkpoint_file(analysis_step='index_variant_calls_status', new_status='complete')


####################################################################################################################
#
# generate_consensus()
#   generate a consensus fasta file from the indexed variant calls
#
####################################################################################################################
def generate_consensus(sample):
    sample.sample_logger.log_section_header('Generating consensus fa file from the indexed variant calls')

    # use the checkpoint file to see if this step has already been completed
    sample.read_checkpoint_file()
    if sample.generate_consensus_status == 'complete':
        sample.sample_logger.log(
            'Checkpoint file indicates that generate_consensus() has already successfully been completed, so skipping this step!')
        return

    # if the checkpoint file isn't marked complete, we need to shred any existing output and start over
    if file_check(file_path=sample.consensus_fa_file, file_or_directory='file'):
        subprocess.run(['rm', str(sample.consensus_fa_file)])

    # generate a consensus fasta file from the indexed variant calls
    with open(sample.consensus_fa_file, 'w') as sample_consensus_fa_file:
        generate_consensus_process = subprocess.run(
            ["bcftools",
             "consensus",
             "-f",
             str(sample.reference_assembly_concatig_file),
             str(sample.filter2_vcf_file)],
            stdout=sample_consensus_fa_file,
            stderr=subprocess.PIPE,
            universal_newlines=True
        )
    sample.sample_logger.log(str(generate_consensus_process.stderr))

    # check the return code from bcftools
    try:
        assert generate_consensus_process.returncode == 0
    except AssertionError:
        sample.sample_logger.log(
            log.bold_red('ERROR: bcftools returned a non-zero returncode for ' + sample.sample_name))
        sample.update_checkpoint_file(analysis_step='generate_consensus_status', new_status='failed')
        raise

    # update the checkpoint file to reflect the status
    sample.update_checkpoint_file(analysis_step='generate_consensus_status', new_status='complete')


####################################################################################################################
#
# rename_fa_file()
#   perform renaming operations on the fa file
#
####################################################################################################################
def rename_fa_file(sample):
    sample.sample_logger.log_section_header('Performing renaming operations on the .fa file')

    # use the checkpoint file to see if this step has already been completed
    sample.read_checkpoint_file()
    if sample.rename_consensus_status == 'complete' and sample.renamed_consensus_file.exists():
        sample.sample_logger.log(
            'Checkpoint file indicates that rename_fa_file() has already successfully been completed, so skipping this step!')
        return

    # if the checkpoint file isn't marked complete, we need to shred any existing output and start over
    if file_check(file_path=sample.renamed_consensus_file, file_or_directory='file'):
        subprocess.run(['rm', str(sample.renamed_consensus_file)])

    # rename inside of the .fa files. The default leaves the reference as the fasta name. We will need to rename it to
    # the Sample_Reference format to interpret the output
    consensus_sequence = ""
    for seq_record in SeqIO.parse(str(sample.consensus_fa_file), "fasta"):
        # consensus_sequence = str(seq_record.seq)
        with open(sample.renamed_consensus_file, 'w') as f:
            if sample.is_reference:
                seq_record.id = sample.sample_name + "_selfmap"
                seq_record.description = sample.sample_name + "_selfmap"
                SeqIO.write(seq_record, f, 'fasta')
            else:
                seq_record.id = sample.sample_name + "_" + sample.reference
                seq_record.description = sample.sample_name + "_" + sample.reference
                SeqIO.write(seq_record, f, 'fasta')

    # update the checkpoint file to reflect the status
    sample.update_checkpoint_file(analysis_step='rename_consensus_status', new_status='complete')


####################################################################################################################
#
# concatenate_group_alignment_files()
#   concatenate all of the .fa output files into group_alignment_file_<timestamp>.fasta, also make a dictionary of
#   groups and their corresponding group_alignment_file for optional gubbins calls
#
####################################################################################################################
def concatenate_group_alignment_files(groups=None, args=None):
    group_alignment_files = OrderedDict()

    for group in groups:

        group_directory = args.output / str(group)

        consensus_sequences = []

        for file in os.scandir(group_directory):
            if file.name.endswith('.renamed_consensus.fna'):
                for seq_record in SeqIO.parse(file.path, "fasta"):
                    consensus_sequences.append(seq_record)

        group_alignment_file = group_directory / (
                str(group) + "_alignment_file_" + datetime.datetime.now().strftime("%Y-%m-%d_%H:%M") + ".fasta")

        group_alignment_files[group] = path_clean(group_alignment_file)

        with open(group_alignment_file, 'w') as group_alignment_file:
            SeqIO.write(consensus_sequences, group_alignment_file, 'fasta')

    return group_alignment_files


####################################################################################################################
#
# run_gubbins()
#   run gubbins for all .fa files in groups that were involved in the current run
#
####################################################################################################################
def run_gubbins(group_alignment_files, args, logger=None):
    logger.log_section_header('Commencing recombination filtering with Gubbins')

    for group, alignment_file in group_alignment_files.items():

        logger.log('\nDetermining the number of samples present in group {} (including self mapping)'.format(group))
        num_samples = len(list(SeqIO.parse(str(alignment_file), "fasta")))
        if num_samples < 4:
            logger.log(
                'Group {} contains {} samples. Gubbins requires FOUR samples, so no recombination filtering will be performed for this group'.format(
                    group, num_samples))
            continue
        else:
            logger.log('Group {} contains {} samples.'.format(group, num_samples))

        # generate a new time-stamped gubbins analysis directory
        gubbins_directory = alignment_file.parent / ('gubbins' + datetime.datetime.now().strftime("%Y-%m-%d_%H:%M"))
        subprocess.run(['mkdir', str(gubbins_directory)])
        logger.log('\nWorking in directory: {}'.format(str(gubbins_directory)))

        # gubbins likes to work where you call it, so we'll change the working directory to the gubbins directory
        os.chdir(gubbins_directory)

        # call gubbins on the copied alignment file
        gubbins_process = subprocess.run(
            ["run_gubbins.py",
             "--threads", str(args.cores),
             str(alignment_file)],
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
            universal_newlines=True
        )

        logger.log(str(gubbins_process.stdout), verbosity=4)

        # check the return code from gubbins
        try:
            assert gubbins_process.returncode == 0
        except AssertionError:
            logger.log(log.bold_red('ERROR: gubbins returned a non-zero returncode for ' + group + ': ' + str(gubbins_process.returncode)))
            logger.log(str(gubbins_process.stderr))