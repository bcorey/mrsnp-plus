#!/usr/bin/env python3
# encoding: utf-8
import json
import os
import shutil
import subprocess
from datetime import datetime

from mrsnp_plus.analysis.gubbins_methods import run_gubbins
from mrsnp_plus.analysis.reference import Reference
from mrsnp_plus.analysis.sample import Sample
from mrsnp_plus.analysis.snippy_methods import snippy_core
from mrsnp_plus.analysis.vcf_methods import index_zip_vcf, merge_vcf, annotate_vcf, generate_annotated_vcf_table, \
    annotation_summary
from mrsnp_plus.common import log
from mrsnp_plus.common.checkpoint import CheckPoint
from mrsnp_plus.common.miscellaneous import file_check, quit_with_error, parallel_compression_handler


class Group:

    def __init__(self, name, output_directory, reads_directory, assembly_directory, samples, reference=None,
                 annotation=None, cores=1, trim=50, filter=500, keep=2, no_gubbins=False):

        self.name = name
        self.output_directory = output_directory
        self.reads_directory = reads_directory
        self.assembly_directory = assembly_directory
        self.group_directory = output_directory / self.name
        self.annotation = annotation
        self.cores = cores
        self.reference_trim = trim
        self.reference_filter = filter
        self.keep = keep
        self.no_gubbins = no_gubbins

        # if the group directory doesn't already exist, make it now
        if not file_check(file_path=self.group_directory, file_or_directory='directory'):
            subprocess.run(['mkdir', str(self.group_directory)])

        # initialize the group logger
        self.logger = self.initialize_logger()

        # initialize the checkpoint Object
        self.checkpoint_directory = self.group_directory / 'checkpoints' / 'group'
        self.checkpoint = CheckPoint(self.name, 'group', self.logger, self.checkpoint_directory)

        # sample_list will hold the names of all of the samples that are in the group. This includes both new samples
        # that are to undergo analysis as well as previously analyzed samples. The former will be populated by the
        # Sample objects while the latter will be added by scanning the group directory for the results of previous
        # analyses.

        self.sample_list = []

        # to scan for old samples, we will first check to see what checkpoint files are present in the samples
        # checkpoint directory

        self.samples_checkpoint_directory = self.checkpoint_directory.parent / 'samples'

        if file_check(file_path=self.samples_checkpoint_directory, file_or_directory='directory'):
            for checkpoint_file in os.scandir(self.samples_checkpoint_directory):
                # attempt to read in the checkpoint file JSON
                with open(checkpoint_file) as chkpnt_file:
                    checkpoint_data = json.load(chkpnt_file)
                    # if the individual analysis is set to complete, this sample completed its analysis and should be
                    # added to the sample_list
                    if checkpoint_data['individual_analysis'] == 'complete':
                        self.sample_list.append(checkpoint_file.name.split('.')[0])

        # we will import the new samples from the passed samples list. Additionally, we will verify that the new samples
        # are indeed new by checking against the current sample_list. We will track new_samples in addition to all
        # samples to keep straight which require analysis and which do not

        self.new_samples = []

        for sample in samples:
            if sample not in self.sample_list:
                self.sample_list.append(sample)
                self.new_samples.append(sample)

        # determine the identity and status of the reference. If the reference is passed when the Group is initialized
        # we can avoid any guess work and assign this to be the reference
        self.reference_name = None

        if reference:
            self.reference_name = reference

        # if the reference is NOT passed, we can try to guess the identity of the reference. This is only possilbe if
        # the group has a preexisting reference directory. In this case we can see if the reference checkpoint file is
        # present and if so if it was successfully processed in the same fashion as with the old samples

        self.reference_directory = self.group_directory / 'reference'
        self.reference_checkpoint_directory = self.checkpoint_directory / 'reference'

        if file_check(file_path=self.reference_checkpoint_directory, file_or_directory='directory'):
            for reference_file in os.scandir(self.reference_checkpoint_directory):
                if 'checkpoint' in reference_file.name:
                    # attempt to read in the checkpoint file JSON
                    with open(reference_file.path) as chkpnt_file:
                        checkpoint_data = json.load(chkpnt_file)
                        # if the individual analysis is set to complete, this sample completed its analysis and should
                        # be added to the sample_list
                        if checkpoint_data['individual_analysis'] == 'complete':
                            self.reference_name = reference_file.name.split('.')[0]

        # if at this point we are still unable to identify the reference for the group, we will abort the run and log
        # the reason
        if not self.reference_name:
            quit_with_error(self.logger, 'unable to determine the reference for group {}'.format(self.name))

    def initialize_logger(self):
        # generate the sample specific log file and add it to the sample attributes

        log_directory = self.group_directory / 'logs'

        if not log_directory.is_dir():
            subprocess.run(['mkdir', str(log_directory)])

        logger = log.Log(
            log_filename=log_directory / (self.name + '.group.mrsnp-plus.log'),
            stdout_verbosity_level=1
        )

        return logger

    def initialize_reference(self):
        # initialize the reference object for this group
        self.reference = Reference(
            self.reference_name,
            self,
            self.assembly_directory,
            self.reads_directory,
            self.cores,
            self.reference_trim,
            self.reference_filter,
            self.annotation
        )

    def process_reference(self):
        self.reference.prepare_reference()
        self.checkpoint.update_checkpoint_file(analysis_step='reference_processing', new_status='complete')

    def initialize_new_samples(self):
        self.new_sample_objects = []
        for new_sample in self.new_samples:
            self.new_sample_objects.append(
                Sample(
                    new_sample,
                    self.group_directory,
                    self.reads_directory,
                    self,
                    self.reference,
                    self.cores
                )
            )
        self.checkpoint.update_checkpoint_file(analysis_step='initialization', new_status='complete')

    def process_new_samples(self):
        for sample in self.new_sample_objects:
            sample.process_sample()
        self.checkpoint.update_checkpoint_file(analysis_step='new_sample_processing', new_status='complete')

    def analysis_preview(self):
        self.logger.log_section_header('Preview of queued analyses for Group {}'.format(self.name))

        self.logger.log('\nThe reference for the selected group is: ' + log.bold_yellow(self.reference_name))
        self.logger.log('\nThe following samples are slated for analysis:')
        for sample in self.new_sample_objects:
            self.logger.log(log.bold_yellow(sample.name))
        self.logger.log('\nThe following samples have already completed analysis:')
        for sample in self.sample_list:
            if sample not in [sample.name for sample in self.new_sample_objects]:
                self.logger.log(log.bold_yellow(sample))

        self.logger.log('\nIn total we will perform new variant identification analyses on ' + log.bold_yellow(
            str(len(self.new_sample_objects))) + ' sample/reference pairs')

        self.logger.log('\nIn total we will perform a summary comparison of annotated variants for ' + log.bold_yellow(
            str(len(self.sample_list))) + ' samples')

    def variant_annotation(self):

        # get the list of all snippy directories in the group
        snippy_directories = []
        sample_vcf_files = []
        for sample in self.sample_list:
            snippy_directory = self.group_directory / 'individual_analyses' / str(sample) / 'snippy_output'
            temp_snippy_directory = snippy_directory.parent / str(sample)
            if file_check(file_path=snippy_directory, file_or_directory='directory'):
                # snippy-core takes the sample name from the directory name. We will rename the snippy output
                # directories to their sample name, then change them back when snippy core is finished
                temp_snippy_directory = snippy_directory.parent / str(sample)
                os.rename(snippy_directory, temp_snippy_directory)
                snippy_directories.append(temp_snippy_directory)
                sample_vcf_file = temp_snippy_directory / (str(sample) + '.vcf')
                sample_vcf_files.append(sample_vcf_file)
            elif file_check(file_path=temp_snippy_directory, file_or_directory='directory'):
                snippy_directories.append(temp_snippy_directory)
                sample_vcf_file = temp_snippy_directory / (str(sample) + '.vcf')
                sample_vcf_files.append(sample_vcf_file)
            else:
                self.logger.log(log.bold_yellow(
                    'WARNING: Sample {} was in the sample list for group {} but no snippy directory was located! It '
                    'will be skipped in the downstream analysis!'.format(sample, str(self.name))
                ))

        # prepare a time-stamped directory for this analysis
        self.analysis_directory = self.group_directory / 'variant_analysis_{}'.format(
            datetime.now().strftime("%Y-%m-%d_%H-%M"))
        self.snippy_core = self.analysis_directory / 'snippy_core'

        # run snippy-core to generate the group alignment file and clean it with snippy-clean
        gubbins_input_file = snippy_core(
            snippy_directories,
            self.reference.annotated_reference_gbk,
            self.snippy_core,
            self.name,
            self.logger,
            self.checkpoint,
            self.cores
        )

        # run gubbins to filter out recombination unless user specified --no-gubbins
        if self.no_gubbins == False:
            self.gubbins_directory = self.analysis_directory / 'recombination_filtering'

            filtered_vcf_files = run_gubbins(
                gubbins_input_file,
                sample_vcf_files,
                self.gubbins_directory,
                self.reference.name,
                self.name,
                self.logger,
                self.checkpoint,
                cores=self.cores
            )
        else:
            filtered_vcf_files = sample_vcf_files

        # set the path to the indexed vcf files directory, and make it
        self.indexed_vcf_directory = self.analysis_directory / 'indexed_vcfs'
        subprocess.run(['mkdir', str(self.indexed_vcf_directory)])

        # compress and index all of the filtered vcf files
        self.logger.log_section_header('indexing and compressing vcf files')
        compressed_indexed_vcf_files = []
        for vcf_file in filtered_vcf_files:
            compressed_indexed_vcf_files.append(
                (index_zip_vcf(
                    vcf_file,
                    self.indexed_vcf_directory,
                    self.name,
                    self.logger
                )))

        # set the path to the variant_annotation directory, and make it
        self.variant_annotation_directory = self.analysis_directory / 'variant_annotation'
        subprocess.run(['mkdir', str(self.variant_annotation_directory)])

        # merge vcf files into a single multi-vcf file
        merged_vcf_file = merge_vcf(
            compressed_indexed_vcf_files,
            self.variant_annotation_directory,
            self.name,
            self.logger
        )

        # annotate the merged vcf file using the reference gff file
        annotated_vcf_file = annotate_vcf(
            merged_vcf_file,
            self.reference.annotated_reference_gff,
            self.variant_annotation_directory,
            self.name,
            self.logger
        )

        # generate the table representing the content of the annotated vcf file
        annotated_vcf_table = generate_annotated_vcf_table(
            annotated_vcf_file,
            self.variant_annotation_directory,
            self.name,
            self.logger
        )

        # generate the summary table that contains the most significant info in a pleasing wee table
        annotation_summary(
            annotated_vcf_table,
            self.sample_list,
            self.name,
            logger=self.logger)

    def clean_up_reference(self):
        # run clean up methods for all of the samples/refererence in the group
        files_for_compression = []

        for file in self.reference.post_analysis_cleanup(keep=self.keep):
            files_for_compression.append(file)

        # run the parallel compression command
        garbage = parallel_compression_handler(
            files_for_compression,
            compress=True,
            compression_type='bzip2',
            logger=self.logger,
            cores=self.cores
        )

    def clean_up_samples(self):
        # run clean up methods for all of the samples/refererence in the group
        files_for_compression = []

        self.logger.log_section_header('Performing post analysis cleanup for new samples')

        for sample in self.new_sample_objects:
            for file in sample.post_analysis_cleanup(keep=self.keep):
                files_for_compression.append(file)

        # run the parallel compression command
        garbage = parallel_compression_handler(
            files_for_compression,
            compress=True,
            compression_type='bzip2',
            logger=self.logger,
            cores=self.cores
        )
