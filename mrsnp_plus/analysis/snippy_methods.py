#!/usr/bin/env python3
# encoding: utf-8
import os
import subprocess

from psutil import virtual_memory

from mrsnp_plus.common import log
from mrsnp_plus.common.miscellaneous import file_check, quit_with_error, path_clean


####################################################################################################################
#
# self_map_reference()
#   use snippy to perform self-mapping of the reference
#
####################################################################################################################
def map_reads(assembly_file, r1_fastq, r2_fastq, output_directory, name, logger, checkpoint, cores=1, min_frac=0.8, min_cov=20):
    logger.log_section_header('Mapping reads to the reference with Snippy')

    # check for a snippy output file (.bam) in the output directory
    output_file = output_directory / (str(name) + '.bam')
    if file_check(file_path=output_file, file_or_directory='file'):
        logger.log('Located a bam file for ' + log.bold_green(str(name)))

        # verify that the file can be trusted using the checkpoint object
        checkpoint.read_checkpoint_file()
        if checkpoint.checkpoint_dict['map_reads'] == 'complete':
            logger.log('checkpoint file indicates that this step was completed. Skipping!')
            return output_file
        else:
            logger.log('checkpoint file does not recognize this step as complete, so the existing output of this step '
                       'will be trashed the analysis redone')

    # ensure that the assembly_file is accessible at the passed path
    try:
        assert file_check(file_path=assembly_file, file_or_directory='file')
        logger.log('Located the assembly file at ' + log.bold_green(str(assembly_file)))
    except AssertionError:
        quit_with_error(logger,
                        'could not locate the raw reference assembly file at: ' + log.bold_red(str(assembly_file)))

    # ensure that the passed read files are accessible
    try:
        assert file_check(file_path=r1_fastq, file_or_directory='file')
        logger.log('Located the r1 read file at ' + log.bold_green(str(r1_fastq)))
    except AssertionError:
        quit_with_error(logger, 'could not locate the r1 read file at: ' + log.bold_red(str(r1_fastq)))
    try:
        assert file_check(file_path=r2_fastq, file_or_directory='file')
        logger.log('Located the r2 read file at ' + log.bold_green(str(r2_fastq)))
    except AssertionError:
        quit_with_error(logger, 'could not locate the r2 read file at: ' + log.bold_red(str(r2_fastq)))

    # check to make sure that the number of cores does not exceed the system available resources
    if cores > len(os.sched_getaffinity(0)):
        quit_with_error(logger,
                        'User requested more cores for this analysis than are currently available.\nThe number of'
                        ' currently available cores is: ' + str(len(os.sched_getaffinity(0))))

    # check to make sure that the output_directory DOESN'T exists. If it does, remove it
    try:
        assert not file_check(file_path=output_directory, file_or_directory='directory')
    except AssertionError:
        subprocess.run(['rm', '-r', str(output_directory)])

    # construct the command line argument as a string for logging purposes
    snippy_cmd = 'snippy --reference {} --R1 {} --R2 {} --minfrac {} --mincov {} --cpus {} --outdir {} --prefix {} --report --unmapped'.format(
        str(assembly_file), str(r1_fastq), str(r2_fastq), str(min_frac), str(min_cov), str(cores), str(output_directory), name
    )

    logger.log('\n Now running snippy with the following command: \n' + log.bold_yellow(snippy_cmd), verbosity=3)

    # run the snippy command using subprocess module

    snippy_process = subprocess.run(
        [
            'snippy',
            '--reference', str(assembly_file),
            '--R1', str(r1_fastq),
            '--R2', str(r2_fastq),
            '--minfrac', str(min_frac),
            '--mincov', str(min_cov),
            '--cpus', str(cores),
            '--outdir', str(output_directory),
            '--prefix', name,
            '--report',
            '--unmapped'
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )

    logger.log('\n' + str(snippy_process.stderr, 'utf-8'), verbosity=3)

    # check the return code from snippy
    try:
        assert snippy_process.returncode == 0
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='map_reads', new_status='failed')
        quit_with_error(logger, 'Snippy returned a non-zero returncode for ' + name + ': ' + str(snippy_process.returncode) + '\n' + str(snippy_process.stderr))

    # validate that the output file was generated where we were expecting it, and if so return it
    try:
        assert file_check(file_path=output_file, file_or_directory='file')
        vcf_file = output_file.parent / (name + '.vcf')
        snippy_vcf_renamer(vcf_file, name, logger)
        checkpoint.update_checkpoint_file(analysis_step='map_reads', new_status='complete')
        return output_file
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='map_reads', new_status='failed')
        quit_with_error(logger,
                        'something went wrong during read mapping. Snippy failed to generate the output bam file for ' + name + ' at the expected path ' + str(
                            output_file))


####################################################################################################################
#
# snippy_core()
#   use snippy-core to generate the core snps files requires for gubbins filtering
#
####################################################################################################################
def snippy_core(sample_snippy_directories, reference, output_directory, group_name, logger, checkpoint, cores=1):
    logger.log_section_header('Using snippy-core to align all of the samples in Group {}'.format(group_name))

    # check for a snippy-core output file (core.full.aln) in the output directory
    output_file = output_directory / 'core.full.aln'
    clean_output_file = output_directory / (str(group_name) + '.clean.core.full.aln')

    if file_check(file_path=clean_output_file, file_or_directory='file'):
        logger.log('Located a clean.core.full.aln file for ' + log.bold_green(str(group_name)))

        # verify that the file can be trusted using the checkpoint object
        checkpoint.read_checkpoint_file()
        if checkpoint.checkpoint_dict['snippy_core_and_clean'] == 'complete':
            logger.log('checkpoint file indicates that this step was completed. Skipping!')
            return clean_output_file
        else:
            logger.log('checkpoint file does not recognize this step as complete, so the existing output of this step '
                       'will be trashed the analysis redone')
            subprocess.run(['rm', '-r', str(output_directory)])

    # verify that the directories in sample_snippy_directories are valid
    for directory in sample_snippy_directories:
        try:
            assert file_check(file_path=directory, file_or_directory='directory')
        except AssertionError:
            quit_with_error(logger, 'could not verify {} for snippy_core()'.format(str(directory)))

    # create the output directory
    subprocess.run(['mkdir', '-p', str(output_directory)])

    # create a space delimited string out of the sample_snippy_directory paths
    snippy_directory_string = ' '.join([str(directory) for directory in sample_snippy_directories])
    snippy_directories_strings = [str(directory) for directory in sample_snippy_directories]

    # verify that the reference gbk path is valid
    try:
        assert file_check(file_path=reference, file_or_directory='file')
    except AssertionError:
        quit_with_error(logger, 'could not verify {} for snippy_core()'.format(str(reference)))

    # construct the command line argument as a string for logging
    snippy_core_cmd = 'snippy-core --ref {} {}'.format(str(reference), snippy_directory_string)

    logger.log('\n Now running snippy-core with the following command: \n' + log.bold_yellow(snippy_core_cmd), verbosity=3)

    # snippy-core, like gubbins, is very naughty and needs to be run explicitly from where the output will be stored
    os.chdir(output_directory)

    # run the snippy-core command using suprocess
    snippy_core_process = subprocess.run(
        [
            snippy_core_cmd
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True
    )

    # check the return code from snippy-core
    try:
        assert snippy_core_process.returncode == 0
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='snippy_core_and_clean', new_status='failed')
        logger.log(str(snippy_core_process.stderr, 'utf-8'))
        # undo the snippy output directory renames
        for snippy_directory in sample_snippy_directories:
            original_snippy_directory = snippy_directory.parent / 'snippy_output'
            os.rename(snippy_directory, original_snippy_directory)
        quit_with_error(logger, 'snippy-core returned a non-zero returncode for ' + group_name + ': ' + str(snippy_core_process.returncode))

    # verify that the required output file was created
    try:
        assert file_check(file_path=output_file, file_or_directory='file')
        checkpoint.update_checkpoint_file(analysis_step='snippy_core_and_clean', new_status='complete')
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='snippy_core_and_clean', new_status='failed')
        # undo the snippy output directory renames
        for snippy_directory in sample_snippy_directories:
            original_snippy_directory = snippy_directory.parent / 'snippy_output'
            os.rename(snippy_directory, original_snippy_directory)
        quit_with_error(logger,
                        'something went wrong during core alignment. snippy-core failed to generate the output file for ' + group_name + ' at the expected path ' + str(
                            output_file))

    # construct the command line argument as a string for logging
    snippy_clean_cmd = 'snippy-clean_full_aln {} > {}'.format(str(output_file), str(clean_output_file))

    logger.log('\n Now running snippy-clean with the following command: \n' + log.bold_yellow(snippy_clean_cmd), verbosity=3)

    # run the snippy-core command using suprocess
    with open(clean_output_file, 'w') as co_file:
        snippy_clean_process = subprocess.run(
            [
                'snippy-clean_full_aln',
                str(output_file)
            ],
            stdout=co_file,
            stderr=subprocess.PIPE
        )

    # check the return code from snippy-clean
    try:
        assert snippy_clean_process.returncode == 0
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='snippy_core_and_clean', new_status='failed')
        quit_with_error(logger, 'snippy-clean returned a non-zero returncode for ' + group_name + ': ' + str(
            snippy_clean_process.returncode))

    # verify that the required output file was created
    try:
        assert file_check(file_path=clean_output_file, file_or_directory='file')
        checkpoint.update_checkpoint_file(analysis_step='snippy_core_and_clean', new_status='complete')
        return clean_output_file
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='snippy_core_and_clean', new_status='failed')
        quit_with_error(logger,
                        'something went wrong during alignment cleaning. snippy-clean failed to generate the output file for ' + group_name + ' at the expected path ' + str(
                            clean_output_file))


####################################################################################################################
#
# snippy_vcf_renamer()
#   snippy makes the sample name field "snippy_output", which will cause us some issues later on when we merge and
#   annotate the vcf files. This method will use sed to change "snippy_output" in the vcf file to the name of the
#   sample
#
####################################################################################################################
def snippy_vcf_renamer(vcf_file, name, logger):
    logger.log('Cleaning up the vcf output file...')

    sed_cmd = "sed -i 's/snippy_output/{}/g' {}".format(name, str(vcf_file))

    sed_process = subprocess.run(
        [
            sed_cmd,
        ],
        stderr=subprocess.PIPE,
        shell=True
    )

    logger.log('\n' + str(sed_process.stderr, 'utf-8'), verbosity=3)

    # check the return code from snippy
    try:
        assert sed_process.returncode == 0
    except AssertionError:
        quit_with_error(logger,
                        'sed returned a non-zero returncode for ' + name + ': ' + str(sed_process.returncode))

