#!/usr/bin/env python3
# encoding: utf-8
import subprocess
import sys

import pandas as pd

from mrsnp_plus.analysis.group import Group
from mrsnp_plus.common import log
from mrsnp_plus.common.miscellaneous import path_clean, file_check, quit_with_error, file_extension_check


####################################################################################################################
#
# analyses_precheck()
#   1) Clean paths for all input files/directories
#   2) Validate the output directory
#   3) Initialize the logger
#
####################################################################################################################
def analyses_precheck(args=None):
    # Ensure that args were passed and clean and validate the output directory
    try:
        assert args is not None
    except AssertionError:
        print(log.bold_red('ERROR: args were not passed to gather_input()!'))
        raise

    try:
        assert args.output
    except AssertionError:
        print(log.bold_red('ERROR: An output directory must be specified!'))
        raise
    args.output = path_clean(args.output)
    try:
        assert file_check(file_path=args.output, file_or_directory='directory')
    except AssertionError:
        subprocess.run(['mkdir', str(args.output)])

    # Initialize the run logger
    logger = log.Log(log_filename=args.output / ('mrsnp_' + args.entry_point + '.log'),
                     stdout_verbosity_level=args.verbosity)
    logger.log_section_header('Validating command line input')
    logger.log('The output directory is: ' + log.green(str(args.output)))
    logger.log('The log file for this run is: ' + log.green(str(logger.log_file_path)))

    # Validate the input files w.r.t the entrypoint passed
    if args.entry_point in ['run', 'check']:

        # Clean and validate the input reads directory
        try:
            assert hasattr(args, 'reads')
        except AssertionError:
            quit_with_error(logger, 'An input directory for reads must be specified!')
        args.reads = path_clean(args.reads)
        try:
            assert file_check(file_path=args.reads, file_or_directory='directory')
        except AssertionError:
            quit_with_error(logger,
                            'Failed to locate the input directory for reads at the specified path: ' + str(args.reads))
        logger.log('reads directory is valid')

        # Clean and validate the input contigs directory
        try:
            assert hasattr(args, 'assemblies')
        except AssertionError:
            quit_with_error(logger, 'An input directory for assemblies must be specified!')
        args.assemblies = path_clean(args.assemblies)
        try:
            assert file_check(file_path=args.assemblies, file_or_directory='directory')
        except AssertionError:
            quit_with_error(logger, 'Failed to locate the input directory for assemblies at the specified path: ' + str(
                args.assemblies))
        logger.log('assemblies directory is valid')

        # In the case where the command is 'check', we will give the default of cores as 1
        if args.entry_point == 'check':
            args.cores = 1

        # Validate input file
        try:
            assert hasattr(args, 'input_file')
        except AssertionError:
            quit_with_error(logger, 'An input file must be specified!')

        args.input_file = path_clean(file_path=args.input_file)

        try:
            assert file_check(file_path=args.input_file, file_or_directory='file')
        except AssertionError:
            quit_with_error(logger, 'Failed to locate input file at specified path: ' + str(args.input_file))
        try:
            assert file_extension_check(file_path=args.input_file, file_extension=['.csv'])
        except AssertionError:
            quit_with_error(logger, 'Input file has the wrong extension (must be .csv)!')

        # validate that the trim and filter parameters are valid
        try:
            assert args.trim >= 0 and args.filter >= 0
        except AssertionError:
            quit_with_error(logger, 'trim and filter must be non-negative integers')

        # initialize groups, a list that will contain all of the analysis group objects
        groups = []

        # import the input_file into a dataframe
        input_file_dataframe = pd.read_csv(str(args.input_file), sep=',')

        # verify that the column names are correct
        list(input_file_dataframe.columns) == ['sample', 'group', 'reference', 'annotation']

        # verify that no rows contain missing values in the sample or group columns
        if pd.isna(input_file_dataframe['sample']).any():
            quit_with_error(logger,
                            'Please verify that every sample in the input file has at least a corresponding sample')
        if pd.isna(input_file_dataframe['group']).any():
            quit_with_error(logger,
                            'Please verify that every group in the input file has at least a corresponding group')

        grouped_by_input_file_dataframe = input_file_dataframe.groupby(['group'])
        for name, subset_dataframe in grouped_by_input_file_dataframe:
            group = str(subset_dataframe['group'].iloc[0])
            samples = [str(sample) for sample in list(subset_dataframe['sample'].values)]
            try:
                reference = str(subset_dataframe['reference'].loc[subset_dataframe['reference'].first_valid_index()])
            except KeyError:
                reference = None
            try:
                annotation = str(subset_dataframe['annotation'].loc[subset_dataframe['annotation'].first_valid_index()])
            except KeyError:
                annotation = None

            groups.append(
                Group(
                    group,
                    args.output,
                    args.reads,
                    args.assemblies,
                    samples,
                    reference=reference,
                    annotation=annotation,
                    cores=args.cores,
                    trim=args.trim,
                    filter=args.filter,
                    keep=args.keep,
                    no_gubbins=args.no_gubbins
                )
            )

        return groups
