#!/usr/bin/env python3
# encoding: utf-8

import os
import subprocess

from mrsnp_plus.common import log
from mrsnp_plus.common.miscellaneous import file_check, quit_with_error


####################################################################################################################
#
# sort_index_bam_file()
#   use samtools to sort and index a bam file
#
####################################################################################################################
def sort_index_bam_file(bam_file, output_directory, name, logger, checkpoint, cores=1):
    logger.log_section_header('Sorting and Indexing bam file with Samtools')

    sorted_bam_file = output_directory / (str(name) + '.bam.sorted')
    sorted_bam_index_file = output_directory / (str(name) + '.bam.sorted.bai')

    # check for a) sorted bam file and b) the associated index file in the output directory
    if file_check(file_path=sorted_bam_file, file_or_directory='file') and file_check(file_path=sorted_bam_index_file,
                                                                                      file_or_directory='file'):
        logger.log('Located a sorted bam file and its associated index for ' + log.bold_green(str(name)))

        # verify that the file can be trusted using the checkpoint object
        checkpoint.read_checkpoint_file()
        if checkpoint.checkpoint_dict['sort_index_bam_file'] == 'complete':
            logger.log('checkpoint file indicates that this step was completed. Skipping!')
            return sorted_bam_file, sorted_bam_index_file
        else:
            logger.log('checkpoint file does not recognize this step as complete, so the existing output of this step '
                       'will be trashed the analysis redone')
            if file_check(file_path=sorted_bam_file, file_or_directory='file'):
                subprocess.run(['rm', str(sorted_bam_file)])
            if file_check(file_path=sorted_bam_index_file, file_or_directory='file'):
                subprocess.run(['rm', str(sorted_bam_index_file)])

    # check to make sure we can see the bam file
    try:
        assert file_check(file_path=bam_file, file_or_directory='file')
        logger.log('Located the assembly file at ' + log.bold_green(str(bam_file)))
    except AssertionError:
        quit_with_error(logger, 'could not locate the raw reference assembly file at: ' + log.bold_red(str(bam_file)))

    # check to make sure that the output_directory exists. If not, make it
    try:
        assert file_check(file_path=output_directory, file_or_directory='directory')
    except AssertionError:
        subprocess.run(['mkdir', str(output_directory)])

    # check to make sure that the number of cores does not exceed the system available resources
    if cores > len(os.sched_getaffinity(0)):
        quit_with_error(logger,
                        'User requested more cores for this analysis than are currently available.\nThe number of'
                        ' currently available cores is: ' + str(len(os.sched_getaffinity(0))))

    # construct the command line argument as a string for logging purposes
    samtools_sort_cmd = 'samtools sort -O bam -o {} -@ {} {}'.format(str(sorted_bam_file), str(cores), str(bam_file))

    logger.log('\n Now running samtools sort with the following command: \n' + log.bold_yellow(samtools_sort_cmd), verbosity=3)

    # run the snippy command using subprocess module
    samtools_sort_process = subprocess.run(
        [
            'samtools',
            'sort',
            '-O', 'bam',
            '-o', str(sorted_bam_file),
            '-@', str(cores),
            str(bam_file)
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )

    logger.log('\n' + str(samtools_sort_process.stderr, 'utf-8'), verbosity=3)

    # check the return code from snippy
    try:
        assert samtools_sort_process.returncode == 0
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='sort_index_bam_file', new_status='failed')
        quit_with_error(logger, 'samtools sort returned a non-zero returncode for ' + name + ': ' + str(
                samtools_sort_process.returncode))

    # validate that the output file was generated where we were expecting it
    try:
        assert file_check(file_path=sorted_bam_file, file_or_directory='file')
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='sort_index_bam_file', new_status='failed')
        quit_with_error(logger,
                        'something went wrong during bam file sorting. Samtools failed to generate the output sorted bam file for ' + name + ' at the expected path ' + str(
                            sorted_bam_file))

    # construct the command line argument as a string for logging purposes
    samtools_index_cmd = 'samtools index -@ {} {}'.format(str(cores - 1), str(sorted_bam_file))

    logger.log('\n Now running samtools index with the following command: \n' + log.bold_yellow(samtools_index_cmd), verbosity=3)

    # run the snippy command using subprocess module
    samtools_index_process = subprocess.run(
        [
            'samtools',
            'index',
            '-@', str(cores - 1),
            str(sorted_bam_file)
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )

    logger.log('\n' + str(samtools_index_process.stderr, 'utf-8'))

    # check the return code from snippy
    try:
        assert samtools_index_process.returncode == 0
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='sort_index_bam_file', new_status='failed')
        quit_with_error(logger, 'samtools index returned a non-zero returncode for ' + name + ': ' + str(
                samtools_index_process.returncode))

    # validate that the output file was generated where we were expecting it
    try:
        assert file_check(file_path=sorted_bam_index_file, file_or_directory='file')
        checkpoint.update_checkpoint_file(analysis_step='sort_index_bam_file', new_status='complete')
        return sorted_bam_file, sorted_bam_index_file
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='sort_index_bam_file', new_status='failed')
        quit_with_error(logger,
                        'something went wrong during bam file sorting. Samtools failed to generate the output sorted bam index file for ' + name + ' at the expected path ' + str(
                            sorted_bam_index_file))

