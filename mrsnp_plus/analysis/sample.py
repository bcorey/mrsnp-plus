#!/usr/bin/env python3
# encoding: utf-8
import csv
import fnmatch
import os
import subprocess
import sys

from mrsnp_plus.analysis.bbtools_methods import read_file_preprocessing, repair_read_order
from mrsnp_plus.analysis.snippy_methods import map_reads
from mrsnp_plus.common import log
from mrsnp_plus.common.miscellaneous import quit_with_error, compression_handler, path_clean, file_check, \
    parallel_compression_handler
from mrsnp_plus.common.checkpoint import CheckPoint


# an object of class sample is used to store all of the file paths relevant to a single sample/reference pair
class Sample:

    def __init__(self, name, group_directory, reads_directory, group, reference, cores=1):

        self.name = name
        self.group_directory = group_directory
        self.checkpoint_directory = self.group_directory / 'checkpoints' / 'samples'
        self.reads_directory = reads_directory
        self.group = group  # OBJECT
        self.reference = reference  # OBJECT
        self.cores = cores

        # initialize the individual analysis directory if necessary
        self.individual_analysis_directory = self.group_directory / 'individual_analyses' / self.name
        if not file_check(file_path=self.individual_analysis_directory, file_or_directory='directory'):
            subprocess.run(['mkdir', '-p', str(self.individual_analysis_directory)])

        # initialize the sample logger
        self.logger, self.log_directory = self.initialize_logger()

        # Initialize a CheckPoint object that will handle all of the checkpoint read/write actions for this sample
        self.checkpoint = CheckPoint(
            name=self.name,
            analysis_type='sample',
            logger=self.logger,
            checkpoint_directory=self.checkpoint_directory
        )

        # determine the paths to the raw read files using scan_for_fastq()
        self.r1_fastq = None
        self.r2_fastq = None
        self.compression_type= None
        self.scan_for_fastq(directory=self.reads_directory)

        # key sample directories
        self.processed_reads_directory = self.individual_analysis_directory / 'processed_reads'
        self.snippy_directory = self.individual_analysis_directory / 'snippy_output'

    def initialize_logger(self):
        # generate the sample specific log file and add it to the sample attributes

        log_directory = self.individual_analysis_directory / 'logs'

        if not log_directory.is_dir():
            subprocess.run(['mkdir', str(log_directory)])

        logger = log.Log(
            log_filename=log_directory / ('{}.{}.mrsnp-plus.log'.format(self.name, self.group.name)),
            stdout_verbosity_level=2
        )

        return logger, log_directory

    def scan_for_fastq(self, directory=None):
        """
        scans the specified directory (default is output/reads/raw_reads/) for fastq files that match the name of the
        sample, then assigns their POSIX paths to the self.r1_fastq and self.r2_fastq object attributes. Finally, method
        returns True if both the r1 and r2 files were located, False if they were not
        """

        for file in os.scandir(directory):
            if (file.name.startswith(self.name + "_") or file.name.startswith(
                    self.name + ".")) and fnmatch.fnmatch(file, '*R1*fastq*'):
                self.r1_fastq = path_clean(file_path=file.path)
            elif (file.name.startswith(self.name + "_") or file.name.startswith(
                    self.name + ".")) and fnmatch.fnmatch(file, '*R2*fastq*'):
                self.r2_fastq = path_clean(file_path=file.path)

        # Error handling for samples that don't have the requisite read files
        if not self.r1_fastq or not self.r2_fastq:
            quit_with_error(self.logger, "one or more fastq files are missing for: " + self.name + "\n")

        # Report the compression type based on the extension on the read files
        if str(self.r1_fastq).endswith('.gz'):
            self.compression_type = 'gzip'
            return
        if str(self.r1_fastq).endswith('.bz2'):
            self.compression_type = 'bzip2'
            return

        # if the read files are NOT compressed, we will set it to bzip2 as the default
        self.compression_type = 'bzip2'
        return

    def post_analysis_cleanup(self, keep=2):
        # method will perform post analysis cleanup for samples THAT COMPLETED ALL ANALYSIS STEPS based on the --keep
        # level submitted by the user. Samples that failed to complete will retain their intermediate files for
        # troubleshooting and restarting the analysis at a viable checkpoint

        self.logger.log(
            'Performing post analysis cleanup for sample {} based on a keep value of {}'.format(self.name, str(keep)), verbosity=3)

        # check to see if the analysis was completed, if not we will skip
        self.checkpoint.read_checkpoint_file()
        if self.checkpoint.checkpoint_dict['individual_analysis'] != 'complete':
            self.logger.log(
                'Sample did NOT make it all the way through analysis, so intermediate files will kept for troubleshooting and reanalyzing')
            return []

        # based on the keep value, we will begin performing cleanup operations
        # we will pass back a list of files that need to be compressed to run that process in parallel
        files_for_compression = []

        # all keep conditions will involve compressing the raw read files
        files_for_compression.append(self.r1_fastq)
        files_for_compression.append(self.r2_fastq)

        if keep == 2:
            # compress the trimmed read files
            for attribute in ['r1_fastq_trimmed', 'r2_fastq_trimmed', 'r1_fastq_sorted', 'r2_fastq_sorted']:
                if hasattr(self, attribute):
                    file = getattr(self, attribute)
                    if file_check(file_path=file, file_or_directory='file'):
                        files_for_compression.append(file)


        if keep == 1 or keep == 0:
            # we will keep all but the largest files (processed reads, bam)
            subprocess.run(['rm', '-r', str(self.processed_reads_directory)])
            subprocess.run(['rm', str(self.snippy_directory / (self.name + '.bam'))])

        if keep == 0:
            # delete all intermediate files that are not essential to future group analysis
            pass

        return files_for_compression

    ####################################################################################################################
    #
    # process_sample()
    #   Scripts all of the tasks involved in processing an individual sample in variant analysis
    #       1) runs read_file_preprocessing() and repair_read_order() to prepare reads for snippy
    #       2) runs map_reads() to generate the reference mapping bam file
    #
    ####################################################################################################################
    def process_sample(self):
        self.logger.log_section_header('Now processing sample {}\n'.format(self.name))

        # (if necessary) decompress the raw read files for this sample
        self.r1_fastq, self.r2_fastq = parallel_compression_handler(
            [self.r1_fastq, self.r2_fastq],
            decompress=True,
            compression_type=self.compression_type,
            logger=self.logger,
            cores=self.cores
        )

        # run read_file_preprocessing() and repair_read_order() to prepare reads for snippy
        self.r1_fastq_trimmed, self.r2_fastq_trimmed = read_file_preprocessing(
            self.r1_fastq,
            self.r2_fastq,
            self.processed_reads_directory,
            self.name,
            self.logger,
            self.checkpoint,
            cores=self.cores
        )

        self.r1_fastq_sorted, self.r2_fastq_sorted, self.unpaired_fastq = repair_read_order(
            self.r1_fastq_trimmed,
            self.r2_fastq_trimmed,
            self.processed_reads_directory,
            self.name,
            self.logger,
            self.checkpoint
        )

        # run map_reads() to generate the self-mapping bam file
        self.bam_file = map_reads(
            self.reference.annotated_reference_gbk,
            self.r1_fastq_sorted,
            self.r2_fastq_sorted,
            self.snippy_directory,
            self.name,
            self.logger,
            self.checkpoint,
            cores=self.cores
        )

        self.checkpoint.update_checkpoint_file(analysis_step='individual_analysis', new_status='complete')
