#!/usr/bin/env python3
# encoding: utf-8
import os
import subprocess

from psutil import virtual_memory

from mrsnp_plus.common import log
from mrsnp_plus.common.miscellaneous import file_check, quit_with_error, path_clean


####################################################################################################################
#
# prokka_annotation()
#   use prokka to annotate an assembly file
#
####################################################################################################################
def prokka_annotation(assembly_file, output_directory, name, logger, checkpoint, proteins=None, cores=1):
    logger.log_section_header('Annotating assembly file using Prokka')

    output_file = output_directory / (str(name) + '.gbk')
    corrected_gbk = output_directory / (str(name) + '.corrected.gbk')
    alt_output_file = output_directory / (str(name) + '.gff')

    # check for the output file in the output directory
    if file_check(file_path=output_file, file_or_directory='file'):
        logger.log('Located a prokka annotated assembly file for ' + log.bold_green(str(name)))

        # verify that the file can be trusted using the checkpoint object
        checkpoint.read_checkpoint_file()
        if checkpoint.checkpoint_dict['prokka_annotation'] == 'complete':
            logger.log('checkpoint file indicates that this step was completed. Skipping!')
            return output_file, alt_output_file
        else:
            logger.log('checkpoint file does not recognize this step as complete, so the existing output of this step '
                       'will be trashed the analysis redone')
            subprocess.run(['rm', '-r', str(output_directory)])

    # ensure that the assembly_file is accessible at the passed path
    try:
        assert file_check(file_path=assembly_file, file_or_directory='file')
        logger.log('Located the assembly file at ' + log.bold_green(str(assembly_file)))
    except AssertionError:
        quit_with_error(logger,
                        'could not locate the raw reference assembly file at: ' + log.bold_red(str(assembly_file)))

    # check to see if a proteins file was passed and if so if the path is valid.
    if proteins:
        try:
            proteins = path_clean(proteins)
            assert file_check(file_path=proteins, file_or_directory='file')
            logger.log('Located the genbank annotation file at ' + log.bold_green(str(proteins)))
        except AssertionError:
            quit_with_error(logger,
                            'could not locate the genbank annotation file at: ' + log.bold_red(str(proteins)))
    else:
        logger.log(log.bold_yellow('WARNING: no reference annotation genbank file was passed for this annotation. The '
                                   'quality of the annotation will suffer accordingly'))

    # check to make sure that the number of cores does not exceed the system available resources
    if cores > len(os.sched_getaffinity(0)):
        quit_with_error(logger,
                        'User requested more cores for this analysis than are currently available.\nThe number of'
                        ' currently available cores is: ' + str(len(os.sched_getaffinity(0))))

    # check to make sure that the output_directory doesn't exist. If  it does, delete it
    try:
        assert not file_check(file_path=output_directory, file_or_directory='directory')
    except AssertionError:
        subprocess.run(['rm', '-r', str(output_directory)])

    # annotate with or without a reference genbank file based on what was provided
    if proteins:

        # write the prokka command to a string for logging
        prokka_cmd = 'prokka --proteins {} --kingdom Bacteria --outdir {} --cpus {} --prefix {} --locustag {} {}'.format(
            str(proteins), str(output_directory), str(cores), name, name, str(assembly_file)
        )

        logger.log('Prokka command: \n' + log.bold_yellow(prokka_cmd), verbosity=3)

        prokka_process = subprocess.run(
            [
                'prokka',
                '--proteins', str(proteins),
                '--kingdom', 'Bacteria',
                '--outdir', str(output_directory),
                '--cpus', str(cores),
                '--prefix', name,
                '--locustag', name,
                str(assembly_file)
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )

    else:
        # write the prokka command to a string for logging
        prokka_cmd = 'prokka --kingdom Bacteria --outdir {} --cpus {} --prefix {} --locustag {} {}'.format(
            str(output_directory), str(cores), name, name, str(assembly_file)
        )

        logger.log('Prokka command: \n' + log.bold_yellow(prokka_cmd), verbosity=3)

        prokka_process = subprocess.run(
            [
                'prokka',
                '--kingdom', 'Bacteria',
                '--outdir', str(output_directory),
                '--cpus', str(cores),
                '--prefix', name,
                '--locustag', name,
                str(assembly_file)
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )


    logger.log('\n' + str(prokka_process.stderr, 'utf-8'), verbosity=3)

    # check the return code from snippy
    try:
        assert prokka_process.returncode == 0
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='prokka_annotation', new_status='failed')
        quit_with_error(logger, 'Prokka returned a non-zero returncode for ' + name + ': ' + str(prokka_process.returncode))
        raise

    # validate that the output file was generated where we were expecting it
    try:
        assert file_check(file_path=output_file, file_or_directory='file')

    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='prokka_annotation', new_status='failed')
        quit_with_error(logger,
                        'something went wrong during error correction. Prokka failed to generate the corrected assembly file for ' + name + ' at the expected path ' + str(
                            output_file))

    #gbk_output_header_correction(output_file, corrected_gbk, logger, checkpoint)

    checkpoint.update_checkpoint_file(analysis_step='prokka_annotation', new_status='complete')
    return output_file, alt_output_file


def gbk_output_header_correction(gbk_file, clean_gbk_file, logger, checkpoint):
    # enforce whitespace exists between the contig ID and the length of the contig in the LOCUS header line

    with open(gbk_file, 'r') as f_in:
        with open(clean_gbk_file, 'a') as f_out:
            for line in f_in.readlines():
                if line.startswith('LOCUS'):
                    line_components = line.strip().split('_')
                    line_components_contig = line_components[1][:11]
                    line_components_end = line_components[1][12:].lstrip()
                    line = '{}_{}\t{}'.format(line_components[0], line_components_contig, line_components_end)
                f_out.write(line)


