#!/usr/bin/env python3
# encoding: utf-8

import subprocess
from pathlib import Path

from mrsnp_plus.common import log
from mrsnp_plus.common.miscellaneous import file_check, quit_with_error


####################################################################################################################
#
# concatenate_assembly()
#   use fuse.sh to concatenate an assembly
#
####################################################################################################################
def concatenate_assembly(assembly_file, output_directory, name, logger, checkpoint):
    logger.log_section_header('Concatenating assembly file using bbtools fuse.sh')

    output_file = output_directory / (str(name) + '.concatenated_assembly.fasta')

    # check for a output file in the output directory
    if file_check(file_path=output_file, file_or_directory='file'):
        logger.log('Located a concatentated assembly file for ' + log.bold_green(str(name)))

        # verify that the file can be trusted using the checkpoint object
        checkpoint.read_checkpoint_file()
        if checkpoint.checkpoint_dict['concatenate_assembly'] == 'complete':
            logger.log('checkpoint file indicates that this step was completed. Skipping!')
            return output_file
        else:
            logger.log('checkpoint file does not recognize this step as complete, so the existing output of this step '
                       'will be trashed the analysis redone')
            subprocess.run(['rm', str(output_file)])

    # ensure that the assembly_file is accessible at the passed path
    try:
        assert file_check(file_path=assembly_file, file_or_directory='file')
        logger.log('Located the assembly file at ' + log.bold_green(str(assembly_file)))
    except AssertionError:
        quit_with_error(logger,
                        'could not locate the raw reference assembly file at: ' + log.bold_red(str(assembly_file)))

    # check to make sure that the output_directory exists. If not, make it
    try:
        assert file_check(file_path=output_directory, file_or_directory='directory')
    except AssertionError:
        subprocess.run(['mkdir', str(output_directory)])

    fuse_command = "fuse.sh in=" + str(assembly_file) + " out=" + str(output_file) + " pad=500 overwrite=t name=" + name

    logger.log('\nFuse command: {}'.format(fuse_command), verbosity=3)

    fuse_process = subprocess.run(
        [fuse_command],
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)

    logger.log(str(fuse_process.stderr, 'utf-8'), verbosity=3)

    # check the return code from fuse
    try:
        assert fuse_process.returncode == 0
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='concatenate_assembly', new_status='failed')
        quit_with_error(logger, 'returned a non-zero returncode for ' + name + ': ' + str(fuse_process.returncode))

    # validate that the output file was generated where we were expecting it
    try:
        assert file_check(file_path=output_file, file_or_directory='file')
        checkpoint.update_checkpoint_file(analysis_step='concatenate_assembly', new_status='complete')
        return output_file
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='concatenate_assembly', new_status='failed')
        quit_with_error(logger,
                        'something went wrong during concatenation. fuse.sh failed to generate the corrected assembly file for ' + name + ' at the expected path ' + str(
                            output_file))


####################################################################################################################
#
# read_file_preprocessing()
#   check to see if the processed, unsorted reads are present
#   check to see if the raw reads are both present
#   run bbduk to generate the processed, unsorted reads for analysis
#
####################################################################################################################
def read_file_preprocessing(r1_fastq, r2_fastq, output_directory, name, logger, checkpoint, cores=1):
    logger.log_section_header('Adapter/Quality Trimming')
    logger.log_explanation('Perform adapter (and light quality) trimming of Illumina read files using bbduk.')

    r1_fastq_trimmed = output_directory / (name + '.r1.trimmed.fastq')
    r2_fastq_trimmed = output_directory / (name + '.r2.trimmed.fastq')

    # check for trimmed fastq files in the output directory
    if file_check(file_path=r1_fastq_trimmed, file_or_directory='file') and file_check(file_path=r2_fastq_trimmed, file_or_directory='file'):
        logger.log('Located trimmed read files for ' + log.bold_green(str(name)))

        # verify that the file can be trusted using the checkpoint object
        checkpoint.read_checkpoint_file()
        if checkpoint.checkpoint_dict['read_file_preprocessing'] == 'complete':
            logger.log('checkpoint file indicates that this step was completed. Skipping!')
            return r1_fastq_trimmed, r2_fastq_trimmed
        else:
            logger.log('checkpoint file does not recognize this step as complete, so the existing output of this step '
                       'will be trashed the analysis redone')
            if file_check(file_path=r1_fastq_trimmed, file_or_directory='file'):
                subprocess.run(['rm', str(r1_fastq_trimmed)])
            if file_check(file_path=r2_fastq_trimmed, file_or_directory='file'):
                subprocess.run(['rm', str(r2_fastq_trimmed)])

    # check to make sure that the output_directory exists. If not, make it
    try:
        assert file_check(file_path=output_directory, file_or_directory='directory')
    except AssertionError:
        subprocess.run(['mkdir', str(output_directory)])

    # verify that we can see the adapter file that should come packaged with the distribution
    adapter_file = Path(__file__).parent.parent / 'resources' / 'adapters.fa'
    try:
        assert file_check(file_path=adapter_file, file_or_directory='file')
    except AssertionError:
        quit_with_error(logger, 'read_file_preprocessing() unable to locate the adapter file at' + str(adapter_file))

    # prepare the adapter trimming cmd as a string
    adapter_trimming_cmd = \
        'bbduk.sh in1={} in2={} out1={} out2={} ref={} ktrim=r k=23 mink=11 hdist=1 tpe tbo qtrim=r trimq=8 mlf=50 -t {}'.format(
            str(r1_fastq), str(r2_fastq), str(r1_fastq_trimmed), str(r2_fastq_trimmed), str(adapter_file), str(cores)
        )

    logger.log('\nbbduk command: {}'.format(adapter_trimming_cmd), verbosity=3)

    # run the adapter trimming command
    bbduk_process = subprocess.run(
        ['bbduk.sh',
         'in1=' + str(r1_fastq),
         'in2=' + str(r2_fastq),
         'out1=' + str(r1_fastq_trimmed),
         'out2=' + str(r2_fastq_trimmed),
         'ref=' + str(adapter_file),
         'ktrim=r',
         'k=23',
         'mink=11',
         'hdist=1',
         'tpe',
         'tbo',
         'qtrim=r',
         'trimq=8',
         'minlen=100',
         '-t=' + str(cores)],
        stderr=subprocess.PIPE,
        universal_newlines=True)

    # log the bbduk stderr to the sample's log file
    bbduk_stderr = bbduk_process.stderr.split('Version')[1].splitlines()
    logger.log('Version ' + bbduk_stderr[0], verbosity=3)
    for line in bbduk_stderr[1:]:
        logger.log(line, verbosity=3)

    # check the return code from bbduk
    try:
        assert bbduk_process.returncode == 0
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='read_file_preprocessing', new_status='failed')
        quit_with_error(logger, 'bbduk returned a non-zero returncode for ' + name + ': ' + str(bbduk_process.returncode))

    # validate that the output file was generated where we were expecting it
    try:
        assert file_check(file_path=r1_fastq_trimmed, file_or_directory='file') and file_check(file_path=r2_fastq_trimmed, file_or_directory='file')
        checkpoint.update_checkpoint_file(analysis_step='read_file_preprocessing', new_status='complete')
        return r1_fastq_trimmed, r2_fastq_trimmed
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='read_file_preprocessing', new_status='failed')
        quit_with_error(logger,
                        'something went wrong during trimming. bbduk failed to generate at least one of the trimmed read files for ' + name)


####################################################################################################################
#
# repair_read_order()
#   call to repair.sh to sort the R1 and R2 files for each sample redirect the **stderr** to a pesort.log file
#   for each sample
#
####################################################################################################################
def repair_read_order(r1_fastq, r2_fastq, output_directory, name, logger, checkpoint):
    logger.log_section_header('Running repair.sh to correct order issues in paired read files')
    logger.log_explanation('Sorting read files into paired order')

    r1_fastq_sorted = output_directory / (name + '.r1.sorted.fastq')
    r2_fastq_sorted = output_directory / (name + '.r2.sorted.fastq')
    unpaired_fastq = output_directory / (name + '.unpaired.fastq')

    # check for trimmed fastq files in the output directory
    if file_check(file_path=r1_fastq_sorted, file_or_directory='file') and \
            file_check(file_path=r2_fastq_sorted, file_or_directory='file') and \
                file_check(file_path=unpaired_fastq, file_or_directory='file'):
        logger.log('Located sorted read files for ' + log.bold_green(str(name)))

        # verify that the file can be trusted using the checkpoint object
        checkpoint.read_checkpoint_file()
        if checkpoint.checkpoint_dict['repair_read_order'] == 'complete':
            logger.log('checkpoint file indicates that this step was completed. Skipping!')
            return r1_fastq_sorted, r2_fastq_sorted, unpaired_fastq
        else:
            logger.log('checkpoint file does not recognize this step as complete, so the existing output of this step '
                       'will be trashed the analysis redone')
            if file_check(file_path=r1_fastq_sorted, file_or_directory='file'):
                subprocess.run(['rm', str(r1_fastq_sorted)])
            if file_check(file_path=r2_fastq_sorted, file_or_directory='file'):
                subprocess.run(['rm', str(r2_fastq_sorted)])
            if file_check(file_path=unpaired_fastq, file_or_directory='file'):
                subprocess.run(['rm', str(unpaired_fastq)])

    repair_command = 'repair.sh in1={} in2={} out1={} out2={} outs={} repair'.format(
        str(r1_fastq), str(r2_fastq), str(r1_fastq_sorted), str(r2_fastq_sorted), str(unpaired_fastq)
        )

    logger.log('\nRepair command: {}'.format(repair_command), verbosity=3)

    repair_process = subprocess.run(
        [repair_command],
        stderr=subprocess.PIPE,
        shell=True,
        universal_newlines=True)

    logger.log(str(repair_process.stderr), verbosity=3)

    # check the return code from repair
    try:
        assert repair_process.returncode == 0
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='repair_read_order', new_status='failed')
        quit_with_error(logger, 'bbduk returned a non-zero returncode for ' + name + ': ' + str(repair_process.returncode))

    # validate that the output file was generated where we were expecting it
    try:
        assert file_check(file_path=r1_fastq_sorted, file_or_directory='file') and \
            file_check(file_path=r2_fastq_sorted, file_or_directory='file') and \
                file_check(file_path=unpaired_fastq, file_or_directory='file')
        checkpoint.update_checkpoint_file(analysis_step='repair_read_order', new_status='complete')
        return r1_fastq_sorted, r2_fastq_sorted, unpaired_fastq
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='repair_read_order', new_status='failed')
        quit_with_error(logger,
                        'something went wrong during read sorting. repair.sh failed to generate at least one of the sorted read files for ' + name)
