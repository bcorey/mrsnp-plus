#!/usr/bin/env python3
# encoding: utf-8
import datetime
import os
import shutil
import subprocess

from Bio import SeqIO


####################################################################################################################
#
# run_gubbins()
#   run gubbins for all of the samples in the group
#
####################################################################################################################
from mrsnp_plus.common.miscellaneous import file_check, quit_with_error


def run_gubbins(group_alignment_file, sample_vcf_files, output_directory, reference, group, logger, checkpoint, cores=1):
    logger.log_section_header('Commencing recombination filtering with Gubbins')

    logger.log('\nDetermining the number of samples present in group {} (including self mapping)'.format(group))
    num_samples = len(list(SeqIO.parse(str(group_alignment_file), "fasta")))-1
    if num_samples < 4:
        logger.log(
            '\nGroup {} contains {} samples. Gubbins requires FOUR samples, so no recombination filtering will be performed for this group'.format(
                group, num_samples))
        checkpoint.update_checkpoint_file(analysis_step='recombination_filtering', new_status='complete')
        return sample_vcf_files
    else:
        logger.log('Group {} contains {} samples.\n'.format(group, num_samples))

    # generate recombination filtering output directory
    subprocess.run(['mkdir', str(output_directory)])
    logger.log('Working in directory: {}'.format(str(output_directory)))

    # gubbins likes to work where you call it, so we'll change the working directory to the gubbins directory
    os.chdir(output_directory)

    # need to copy the alignment file into the working directory
    copied_alignment_file = output_directory / str(group_alignment_file.name)
    shutil.copy(group_alignment_file, copied_alignment_file)

    # call gubbins on the copied alignment file
    gubbins_process = subprocess.run(
        ["run_gubbins.py",
         '--prefix', str(group),
         "--threads", str(cores),
         '--outgroup', str(reference),
         str(copied_alignment_file)],
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        universal_newlines=True
    )

    logger.log(str(gubbins_process.stdout), verbosity=3)

    # check the return code from gubbins
    try:
        assert gubbins_process.returncode == 0
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='recombination_filtering', new_status='failed')
        quit_with_error(logger, 'gubbins returned a non-zero returncode for ' + group + ': ' + str(
            gubbins_process.returncode) + ":" + gubbins_process.stderr)

    # verify that the required output file was created
    output_file = output_directory / (str(group) + '.recombination_predictions.gff')

    try:
        assert file_check(file_path=output_file, file_or_directory='file')
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='recombination_filtering', new_status='failed')
        quit_with_error(logger,
                        'something went wrong during recombination filtering. gubbins failed to generate the output file for ' + group + ' at the expected path ' + str(
                            output_file))

    # verify that any recombination was performed. If not, log it and return the unfiltered sample vcf file paths
    if len(open(output_file).readlines()) < 3:
        logger.log('\nNOTE: gubbins detected no recombinant regions to filter!')
        return sample_vcf_files

    # make a subdirectory for the split gff files
    split_gff_directory = output_directory / 'filtering_files'
    subprocess.run(['mkdir', str(split_gff_directory)])

    # run split_gff()
    gff_files = split_gff(output_file, split_gff_directory)

    # convert gff files to bed files
    bed_files = convert_gff_to_bed(gff_files, group, logger, checkpoint)

    # reformat the bed files
    cleaned_bed_files = reformat_bed_files(bed_files, reference, logger, checkpoint)

    # filter recombination for isolates with a clean bed file
    filtered_vcf_files = mask_recombinant_regions(split_gff_directory, sample_vcf_files, group, logger, checkpoint)

    checkpoint.update_checkpoint_file(analysis_step='recombination_filtering', new_status='complete')
    return filtered_vcf_files



####################################################################################################################
#
# split_gff()
#   split the gff output from gubbins into individual samples
#
####################################################################################################################
def split_gff(gubbins_output, output_directory):
    gff_files = []
    gff_taxa_dict = {}
    header = ""
    with open(str(gubbins_output), "r") as f:
        for line in f:
            # copy commented lines to the header of each file
            if line.startswith("#"):
                header += line
                continue
            # parsing down to the list of MRSN numbers for a given region (line in gff file)
            tax_id_list = line.strip().split("\t")[8]
            tax_id_list = tax_id_list.split(";")
            tax_id_list = tax_id_list[2]
            tax_id_list = tax_id_list[5:]
            tax_id_list = tax_id_list.strip().strip("\"").strip().split()

            # iterate through each MRSN####
            for sample in tax_id_list:
                # skip gubbins "Reference", reference MRSN should already be included as MRSN####!
                if sample == "Reference":
                    continue
                sample = sample.strip()
                if sample not in gff_taxa_dict:
                    print("Gubbins predicts recombination for sample: " + sample)
                    gff_taxa_dict[sample] = []
                gff_taxa_dict[sample].append(line)

    # Write a file for each sample with homologous recombination
    for sample, lines in gff_taxa_dict.items():
        file_handle_out = output_directory / (str(sample) + "_split.gff")
        with open(file_handle_out, "w") as f:
            f.write(header)
            for line in lines:
                f.write(line)
        gff_files.append(file_handle_out)

    return gff_files

####################################################################################################################
#
# convert_gff_to_bed()
#   convert .gff to .beds (gff2bed from BEDOPS)
#
####################################################################################################################
def convert_gff_to_bed(gff_files, group, logger, checkpoint):

    bed_files = []

    for file in gff_files:
        bed_file = file.parent / (str(file.name).split('_split')[0] + '.bed.raw')
        convert_gff_to_bed_process = subprocess.run(
            [
                'gff2bed < ' + str(file) + '> ' + str(bed_file)
            ],
            shell=True,
            stderr=subprocess.PIPE
        )

        logger.log('\n' + str(convert_gff_to_bed_process.stderr, 'utf-8'), verbosity=3)

        # check the return code from gff2bed
        try:
            assert convert_gff_to_bed_process.returncode == 0
        except AssertionError:
            checkpoint.update_checkpoint_file(analysis_step='recombination_filtering', new_status='failed')
            quit_with_error(logger, 'gff2bed returned a non-zero returncode for ' + group + ': ' + str(
                convert_gff_to_bed_process.returncode))

        # verify that the required output file was created
        try:
            assert file_check(file_path=bed_file, file_or_directory='file')
        except AssertionError:
            checkpoint.update_checkpoint_file(analysis_step='recombination_filtering', new_status='failed')
            quit_with_error(logger,
                            'something went wrong converting gff files to bed files. bedops failed to generate the output file for ' + group + ' at the expected path ' + str(
                                bed_file))

        bed_files.append(bed_file)

    return bed_files


####################################################################################################################
#
# reformat_bed_files()
#   rename bed files to MRSN####.bed
# 	add dummy header to bed files (removed during gff2bed)
# 	fix identifier in bed files to name of reference contig in .vcf
#
####################################################################################################################
def reformat_bed_files(raw_bed_files, reference, logger, checkpoint):

    cleaned_bed_files = []

    for raw_bed_file in raw_bed_files:

        cleaned_bed_file = raw_bed_file.parent / '.'.join(str(raw_bed_file.name).split('.')[:-1])

        with open(str(cleaned_bed_file), 'w') as f:

            header_line_1 = 'track name=pairedReads'

            f.write(header_line_1)

            with open(str(raw_bed_file), 'r') as raw:

                for line in raw.readlines():

                    line_components = line.split('\t')
                    line_components[0] = reference
                    f.write('\t'.join(line_components))

        cleaned_bed_files.append(cleaned_bed_file)

    return cleaned_bed_files


####################################################################################################################
#
# mask recombinant regions()
#   mask recombinant regions in vcf files using the reformatted bed files
#
####################################################################################################################
def mask_recombinant_regions(bed_file_directory, vcf_files, group, logger, checkpoint):

    filtered_vcf_files = []

    for vcf_file in vcf_files:

        # check to see if there is a bed file for this sample to be filtered with. If not skip filtering
        sample_name = vcf_file.name.split('.')[0]
        bed_file = bed_file_directory / (sample_name + '.bed')

        if not file_check(file_path=bed_file, file_or_directory='file'):
            filtered_vcf_files.append(vcf_file)
            continue

        # use vcftools to filter recombinant regions, and save to a new vcf file
        filtered_vcf_file = bed_file.parent / (sample_name + '.recode.vcf')
        filtered_vcf_file_prefix = str(filtered_vcf_file)[:-11]

        vcftools_process = subprocess.run(
            [
                'vcftools',
                '--vcf', str(vcf_file),
                '--exclude-bed', str(bed_file),
                '--recode',
                '--recode-INFO-all',
                '--out', str(filtered_vcf_file_prefix)
            ],
            stderr=subprocess.PIPE
        )

        logger.log('\n' + str(vcftools_process.stderr, 'utf-8'), verbosity=3)

        # check the return code from gff2bed
        try:
            assert vcftools_process.returncode == 0
        except AssertionError:
            checkpoint.update_checkpoint_file(analysis_step='recombination_filtering', new_status='failed')
            quit_with_error(logger, 'vcftools returned a non-zero returncode for ' + group + ': ' + str(
                vcftools_process.returncode))

        # verify that the required output file was created
        try:
            assert file_check(file_path=filtered_vcf_file, file_or_directory='file')
        except AssertionError:
            checkpoint.update_checkpoint_file(analysis_step='recombination_filtering', new_status='failed')
            quit_with_error(logger,
                            'something went wrong filtering the vcf file to remove recombination. vcftools failed to generate the output file for ' + group + ' at the expected path ' + str(
                                filtered_vcf_file))

        filtered_vcf_files.append(filtered_vcf_file)

    return filtered_vcf_files









