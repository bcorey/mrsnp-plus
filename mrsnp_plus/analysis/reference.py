#!/usr/bin/env python3
# encoding: utf-8
import fnmatch
import os
from collections import OrderedDict
import subprocess
from pathlib import Path

from Bio import SeqIO

from mrsnp_plus.analysis.bbtools_methods import read_file_preprocessing, repair_read_order, concatenate_assembly
from mrsnp_plus.analysis.pilon_methods import polish_assembly
from mrsnp_plus.analysis.prokka_methods import prokka_annotation
from mrsnp_plus.analysis.samtools_methods import sort_index_bam_file
from mrsnp_plus.analysis.snippy_methods import map_reads
from mrsnp_plus.common import log
from mrsnp_plus.common.checkpoint import CheckPoint
from mrsnp_plus.common.miscellaneous import quit_with_error, file_check, path_clean, compression_handler, \
    parallel_compression_handler


class Reference:

    def __init__(self, name, group, assembly_directory, reads_directory, cores, trim, filter, annotation=None):

        self.name = name
        self.group = group  # OBJECT
        self.group_directory = self.group.group_directory
        self.assembly_directory = assembly_directory
        self.reads_directory = reads_directory
        self.cores = cores
        self.trim = trim
        self.filter = filter
        self.annotation = annotation

        # already_processed lets the analysis know that the reference was not built during this run
        self.already_processed = False

        # initialize the reference logger
        self.logger, self.reference_directory = self.initialize_logger()

        # initialize the checkpoint Object
        self.checkpoint_directory = self.group_directory / 'checkpoints' / 'reference'
        self.checkpoint = CheckPoint(self.name, 'reference', self.logger, self.checkpoint_directory)

        # determine if the raw assembly file for the reference exists in the assembly_directory
        self.assembly_file = None
        self.scan_for_assembly(directory=self.assembly_directory)

        # determine if the raw read files are present for the reference
        self.r1_fastq = None
        self.r2_fastq = None
        self.compression_type = None
        self.scan_for_fastq(directory=self.reads_directory)

        # key reference directories
        self.processed_reads_directory = self.reference_directory / 'processed_reads'
        self.snippy_directory = self.reference_directory / 'snippy_output'
        self.samtools_directory = self.reference_directory / 'samtools_output'
        self.pilon_directory = self.reference_directory / 'pilon_output'
        self.prokka_directory = self.reference_directory / 'prokka_output'

    def initialize_logger(self):
        # generate the sample specific log file and add it to the sample attributes

        reference_directory = self.group_directory / 'reference'

        if not reference_directory.is_dir():
            subprocess.run(['mkdir', str(reference_directory)])

        logger = log.Log(log_filename=reference_directory / (self.name + '.reference.mrsnp-plus.log'),
                                   stdout_verbosity_level=2)

        return logger, reference_directory

    def scan_for_assembly(self, directory=None):
        """
        scans the specified directory (default is output/assembly/<sample name>/) for fna file that match the name of the
        sample, then assigns the POSIX path to the self.assembly object attributes
        """

        if not self.checkpoint.checkpoint_dict['individual_analysis'] == 'complete':

            # search the directory for files that start with the sample name and end with/include a common fasta extension
            for file in os.scandir(directory):
                if (file.name.startswith(self.name + '_') or file.name.startswith(
                        self.name + '.')) and (
                        fnmatch.fnmatch(file, '*.fasta*') or fnmatch.fnmatch(file, '*.fna*')):
                    self.assembly_file = Path(file.path).expanduser().resolve()
                    return

            quit_with_error(self.logger,
                            "You are missing the assembly file for the reference: " + self.name + "\n")

    def scan_for_fastq(self, directory=None):
        """
        scans the specified directory (default is output/reads/raw_reads/) for fastq files that match the name of the
        sample, then assigns their POSIX paths to the self.r1_fastq and self.r2_fastq object attributes. Finally, method
        returns True if both the r1 and r2 files were located, False if they were not
        """

        if not self.checkpoint.checkpoint_dict['individual_analysis'] == 'complete':

            for file in os.scandir(directory):
                if (file.name.startswith(self.name + "_") or file.name.startswith(
                        self.name + ".")) and fnmatch.fnmatch(file, '*R1*fastq*'):
                    self.r1_fastq = path_clean(file_path=file.path)
                elif (file.name.startswith(self.name + "_") or file.name.startswith(
                        self.name + ".")) and fnmatch.fnmatch(file, '*R2*fastq*'):
                    self.r2_fastq = path_clean(file_path=file.path)

            # Error handling for references that don't have the requisite read files
            if not self.r1_fastq or not self.r2_fastq:
                quit_with_error(self.logger,
                                "one or more fastq files are missing for: " + self.name + "\n")

            # Report the compression type based on the extension on the read files
            if str(self.r1_fastq).endswith('.gz'):
                self.compression_type = 'gzip'
                return
            if str(self.r1_fastq).endswith('.bz2'):
                self.compression_type = 'bzip2'
                return

            # if the read files are NOT compressed, we will set it to bzip2 as the default
            self.compression_type = 'bzip2'
            return

    ####################################################################################################################
    #
    # prepare_reference()
    #   Scripts all of the tasks involved in preparing a reference for use in variant analysis
    #       1) runs preprocess_assembly() to perform trimming and filtering of the raw assembly file
    #       2) runs read_file_preprocessing() and repair_read_order() to prepare reads for snippy
    #       3) runs map_reads() to generate the self-mapping bam file
    #       4) runs sort_index_bam_file() to prepare the bam file output of snippy for pilon
    #       5) runs polish_assembly() to perform error correction of the assembly
    #       6) runs concatenate_assembly() to generate a concatig from the polished assembly
    #       7) runs prokka_annotation() to annotate the polished concatig
    #
    ####################################################################################################################
    def prepare_reference(self):

        self.logger.log_section_header('Now preparing the reference for group {}'.format(self.group.name))

        # use the checkpoint file to see if the reference is already complete. If so, set just the minimum viable set of
        # reference paths for output files and return
        if self.checkpoint.checkpoint_dict['individual_analysis'] == 'complete':
            self.annotated_reference_gbk = self.prokka_directory / (str(self.name) + '.gbk')
            self.annotated_reference_gff = self.prokka_directory / (str(self.name) + '.gff')

            if not file_check(file_path=self.annotated_reference_gbk, file_or_directory='file') and file_check(file_path=self.annotated_reference_gff, file_or_directory='file'):
                quit_with_error(self.logger, 'something is wrong with the reference directory. The checkpoint file '
                                             'claims that it has completed processing but either the annotated .gbk or '
                                             '.gff file could not be located')

            self.already_processed = True

            self.logger.log('Reference has already been processed, ready to analyze new samples')

            return

        # run preprocess_assembly() to perform trimming and filtering of the raw assembly file
        self.preprocessed_assembly = preprocess_assembly(
            self.assembly_file,
            self.reference_directory,
            self.name,
            self.logger,
            self.checkpoint,
            contig_trim=self.trim,
            contig_filter=self.filter
        )

        # (if necessary) decompress the raw read files for this sample
        self.r1_fastq, self.r2_fastq = parallel_compression_handler(
            [self.r1_fastq, self.r2_fastq],
            decompress=True,
            compression_type=self.compression_type,
            logger=self.logger,
            cores=self.cores
        )

        # run read_file_preprocessing() and repair_read_order() to prepare reads for snippy
        self.r1_fastq_trimmed, self.r2_fastq_trimmed = read_file_preprocessing(
            self.r1_fastq,
            self.r2_fastq,
            self.processed_reads_directory,
            self.name,
            self.logger,
            self.checkpoint,
            cores=self.cores
        )

        self.r1_fastq_sorted, self.r2_fastq_sorted, self.unpaired_fastq = repair_read_order(
            self.r1_fastq_trimmed,
            self.r2_fastq_trimmed,
            self.processed_reads_directory,
            self.name,
            self.logger,
            self.checkpoint
        )

        # run map_reads() to generate the self-mapping bam file
        self.bam_file = map_reads(
            self.preprocessed_assembly,
            self.r1_fastq_sorted,
            self.r2_fastq_sorted,
            self.snippy_directory,
            self.name,
            self.logger,
            self.checkpoint,
            cores=self.cores
        )

        # run sort_index_bam_file() to prepare the bam file output of snippy for pilon
        self.sorted_bam_file, self.sorted_bam_index_file = sort_index_bam_file(
            self.bam_file,
            self.samtools_directory,
            self.name,
            self.logger,
            self.checkpoint,
            cores=self.cores
        )

        # run polish_assembly() to perform error correction of the assembly
        self.pilon_polished_assembly = polish_assembly(
            self.preprocessed_assembly,
            self.sorted_bam_file,
            self.pilon_directory,
            self.name,
            self.logger,
            self.checkpoint,
            cores=self.cores
        )

        # run concatenate_assembly() to generate a concatig from the polished assembly
        self.concatenated_polished_assembly = concatenate_assembly(
            self.pilon_polished_assembly,
            self.reference_directory,
            self.name,
            self.logger,
            self.checkpoint
        )

        # run prokka_annotation() to annotate the polished assembly
        self.annotated_reference_gbk, self.annotated_reference_gff = prokka_annotation(
            self.concatenated_polished_assembly,
            self.prokka_directory,
            self.name,
            self.logger,
            self.checkpoint,
            proteins=self.annotation,
            cores=self.cores)

        # update the checkpoint file to denote that this process is complete
        self.checkpoint.update_checkpoint_file(analysis_step='individual_analysis', new_status='complete')

    def post_analysis_cleanup(self, keep=2):
        # method will perform post analysis cleanup for samples THAT COMPLETED ALL ANALYSIS STEPS based on the --keep
        # level submitted by the user. Samples that failed to complete will retain their intermediate files for
        # troubleshooting and restarting the analysis at a viable checkpoint

        # check to see if the analysis was completed, if not we will skip
        self.checkpoint.read_checkpoint_file()
        if self.checkpoint.checkpoint_dict['individual_analysis'] != 'complete':
            self.logger.log(
                'Sample did NOT make it all the way through analysis, so intermediate files will kept for troubleshooting and reanalyzing')
            return []

        # check to see if self.already_processed is True. In that case no cleanup should be required
        if self.already_processed == True:
            return []

        self.logger.log_section_header(
            'Performing post analysis cleanup of the reference based on a keep value of {}'.format(str(keep)))

        # based on the keep value, we will begin performing cleanup operations
        # we will pass back a list of files that need to be compressed to run that process in parallel
        files_for_compression = []

        # all keep conditions will involve compressing the raw read files
        files_for_compression.append(self.r1_fastq)
        files_for_compression.append(self.r2_fastq)

        if keep == 2:
            # compress the trimmed read files
            for attribute in ['r1_fastq_trimmed', 'r2_fastq_trimmed', 'r1_fastq_sorted', 'r2_fastq_sorted']:
                if hasattr(self, attribute):
                    file = getattr(self, attribute)
                    if file_check(file_path=file, file_or_directory='file'):
                        files_for_compression.append(file)

        if keep == 1 or keep == 0:
            # we will keep all but the largest files (processed reads, bam)
            subprocess.run(['rm', '-r', str(self.processed_reads_directory)])
            subprocess.run(['rm', '-r', str(self.snippy_directory)])
            subprocess.run(['rm', '-r', str(self.pilon_directory)])
            subprocess.run(['rm', '-r', str(self.samtools_directory)])

        if keep == 0:
            # delete all intermediate files that are not essential to future group analysis
            pass

        return files_for_compression


####################################################################################################################
#
# preprocess_assemby()
#   use biopython to perform reference preprocessing actions:
#       - remove contings less than 500bp long
#       - trim 50bp from both ends of the contigs
#
####################################################################################################################
def preprocess_assembly(assembly_file, output_directory, name, logger, checkpoint, contig_trim=50, contig_filter=500):
    logger.log_section_header('Preprocessing Assembly File')

    # clean input just to be safe
    assembly_file = path_clean(file_path=assembly_file)
    output_directory = path_clean(file_path=output_directory)

    output_file = output_directory / (str(name) + '.preprocessed_assembly.fasta')

    # check to see if the preprocessed assembly file is already present
    if file_check(file_path=output_file, file_or_directory='file'):
        logger.log('Located a preprocessed assembly file for ' + log.bold_green(str(name)))

        # verify that the file can be trusted using the checkpoint object
        checkpoint.read_checkpoint_file()
        if checkpoint.checkpoint_dict['preprocess_assembly'] == 'complete':
            logger.log('checkpoint file indicates that this step was completed. Skipping!')
            return output_file
        else:
            logger.log('checkpoint file does not recognize this step as complete, so the existing output of this step '
                       'will be trashed the analysis redone')
            subprocess.run(['rm', str(output_file)])

    # ensure that the raw assembly file is accessible at the path stored at reference.assembly_file
    try:
        assert file_check(file_path=assembly_file, file_or_directory='file')
        logger.log('Located the raw reference assembly file at ' + log.bold_green(str(assembly_file)))
    except AssertionError:
        quit_with_error(logger,
                        'could not locate the raw reference assembly file at: ' + log.bold_red(str(assembly_file)))

    # perform filtering and trimming actions using biopython
    logger.log('Filtering and trimming reference assembly:')
    logger.log('\tFiltering out contigs less than: ' + log.bold_green(str(contig_filter)) + ' bp')
    logger.log('\tTrimming remaining contigs: ' + log.bold_green(str(contig_trim)) + ' bp from both ends')

    filtered_sequences = OrderedDict()
    filtered_contig_count = 0
    for seq_record in SeqIO.parse(str(assembly_file), "fasta"):
        sequence = str(seq_record.seq).upper()
        if len(sequence) >= contig_filter + 2*contig_trim:
            if contig_trim != 0:
                filtered_sequences[seq_record.id] = sequence[contig_trim:-contig_trim]
            else:
                filtered_sequences[seq_record.id] = sequence
        else:
            filtered_contig_count += 1
    with open(output_file, 'w') as clean_assembly:
        for key, value in filtered_sequences.items():
            clean_assembly.write(">  " + key + '\n' + value + '\n')

    logger.log('\nFiltered ' + log.bold_green(str(filtered_contig_count)) + ' contigs from the raw assembly file')
    logger.log('Preprocessed assembly file stored at ' + log.bold_green(str(output_file)))

    # validate that the output file was generated where we were expecting it
    try:
        assert file_check(file_path=output_file, file_or_directory='file')
        checkpoint.update_checkpoint_file(analysis_step='preprocess_assembly', new_status='complete')
        return output_file
    except AssertionError:
        checkpoint.update_checkpoint_file(analysis_step='preprocess_assembly', new_status='failed')
        quit_with_error(logger,
                        'something went wrong during error correction. Pilon failed to generate the corrected assembly file for ' + name + ' at the expected path ' + str(
                            output_file))
