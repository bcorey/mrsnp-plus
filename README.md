# MRsnp+

MRSN bacterial variant analysis and annotation pipeline

## Contents
* [Introduction](#introduction)
* [Commands](#commands)
  * [check](#check)
  * [run](#run)
  * [version](#version)
* [Installation](#installation)
  * [Conda Installation](#conda-installation)
* [Usage](#usage)

## Introduction

MRsnp+ is a ground up rebuild of MRsnp, the previous pipeline for Single Nucleotide Polymorphism
(SNP) detection. 

While the earlier iteration made use of bowtie2 and samtools/bcftools for variant detection, MRsnp+
employs [snippy](https://github.com/tseemann/snippy) for variant detection. As a result the results
are generated faster and we are able to provide more comprehesive output

The workflow can be broken down into 4 processes:

1. __Validate the requested analyses and prepare a preview__ (refer to command 'check')
 - read the information on the [input file](#input-file) (.csv) that defines the analysis structure
 - determine if the required read/assembly/annotation files are present to complete the requested analysis
 - provide a "preview" of what analyses will be conducted to the terminal window

2. __Process the reference for the current group__

 - Trimming and filtering the reference assembly
of small contigs
 - Using [bbduk](https://jgi.doe.gov/data-and-tools/bbtools/bb-tools-user-guide/bbduk-guide/) to
trim adapters and low quality sequence from the reference read files
 - Self-mapping the trimmed reference reads to the filtered reference assembly using [snippy](https://github.com/tseemann/snippy)
 - Sorting and indexing the resulting bam file using [samtools](http://www.htslib.org/)
 - Error-correcting the reference with [pilon](https://github.com/broadinstitute/pilon/wiki)
 - Concatenating the error-corrected reference assembly with [fuse](https://jgi.doe.gov/data-and-tools/bbtools/bb-tools-user-guide/reformat-guide/)
 - Annotating the concatenated reference with [prokka](https://github.com/tseemann/prokka) provided with a high quality reference annotation from genbank

3. __Process all NEW samples in the group individually__

- Using [bbduk](https://jgi.doe.gov/data-and-tools/bbtools/bb-tools-user-guide/bbduk-guide/) to
trim adapters and low quality sequence from the sample read files
 - Mapping the trimmed reference reads to the filtered reference assembly using [snippy](https://github.com/tseemann/snippy)

4. __Perform comparative variant analysis__
 - Use snippy-core and snippy-clean_full_aln to generate a whole genome alignment of ALL samples in the group
 (new samples, existing samples, and the self-mapped reference)
 - Filter recombination using [gubbins](https://github.com/sanger-pathogens/gubbins), 
 [bedops](https://bedops.readthedocs.io/en/latest/), 
 and [vcf-tools](https://vcftools.github.io/index.html)
 - Compress, index, and merge the filtered vcf files with [bgzip](http://www.htslib.org/doc/bgzip.html), 
 [tabix](http://www.htslib.org/doc/tabix.html),
 and [vcf-merge](https://vcftools.github.io/perl_module.html)
 - Transfer annotation information from the reference .gff file to the multi-vcf file, and then summarize this information as output for the user

## Input-file

The input file is a csv that describes the analyses that the user plans to execute. The format is as follows:

```
sample, group, reference, annotation,
sample_a, group_a, sample_a, /path/to/closely/related/gbk/file,
sample_b, group_a, sample_a, /path/to/closely/related/gbk/file,
sample_c, group_a, sample_a, /path/to/closely/related/gbk/file,
```

Essentially, you are defining the group for all samples, as well as the reference and the annotation
file that will be used with that group.

In the case that you are processing additional samples in an EXISTING group, you can leave the reference and annotation 
fields blank for each line; MRsnp+ will supply that information when the existing group is detected


## Commands

### check
```

```

### run
```

```

### version